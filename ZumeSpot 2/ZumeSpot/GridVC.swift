//
//  GridVC.swift
//  ZumeSpot
//
//  Created by mrinal khullar on 8/23/16.
//  Copyright © 2016 mrinal khullar. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class GridVC: UIViewController ,UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
{
    
    @IBOutlet weak var collection_view: UICollectionView!
    @IBOutlet var loadmorebutton: UIButton!
    var imagearray:[UIImage]=[]
    var instaimgurl=NSString()
    var hotelname=NSString()
    var itemsdict = NSMutableDictionary()
    var namearray=NSMutableArray()
    var hotelnamearr=NSMutableArray()
    
    var instagramid=NSMutableArray()
    
    var imagearray1=NSMutableArray()
    
    var sorteddistance1=[AnyObject]()
    var sorteddistance2=NSMutableArray()
    var position=0
    var footerhidden1=0
    var footerhidden2=0
    var lastindex1=15
    var lastindex2=15
    var demoarr=["18790254","29405118","183002827","4955427","203260730"]
    //,"219328630","966497863","1754000114","490579557","11498596","2942791303","264145867","448748183","248390809","227979454","222516979","311731104","32933935","1517504360","1054780910","461532137","287698280","246346875","599894690","201996130","282567309"]
    
    
    
    var nameofhotels = NSMutableArray()
    var latitudeofhotels = NSMutableArray()
    var longitudeofhotels = NSMutableArray()
    var distanceofhotels = NSMutableArray()
    
    var sorteddistance=[AnyObject]()
    
    var dictionaryofname = NSMutableDictionary()
    var dictionaryofinstagramid = NSMutableDictionary()
    var dictionaryoftwitterusername = NSMutableDictionary()
    var dictionaryoffacebookid = NSMutableDictionary()
    
    var nameofhotelssorted = NSMutableArray()

    var instagramnum = 1
    var twitternum = 1
    var facebooknum = 1
    
    var position1 = 0
    
    var countofhotel = 10
    
    var countoffooter = 0
    
    var sorteddictarray = Dictionary<String, Double>()
    
    var distancetoshowongrid = NSMutableArray()
    
    var accesstoken = String()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        
        self.navigationController?.navigationBarHidden=true
       // imagearray=[UIImage(named: "hoteldefault")!,UIImage(named: "splashscreen")!]
        
        let layout = UICollectionViewFlowLayout()
        
        if (UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad)
        {
            
            layout.minimumInteritemSpacing=4
            layout.minimumLineSpacing=4
            layout.sectionInset.left=4
            layout.sectionInset.right=4
            
            
            countofhotel = 20
        }
        else
        {
            layout.minimumInteritemSpacing=2
            layout.minimumLineSpacing=2
            layout.sectionInset.left=2
            layout.sectionInset.right=2
        }
        
        
        collection_view.collectionViewLayout=layout
        
        
        
        let imageView=UIImageView(frame: CGRectMake(20, 38, 20, 15))
        imageView.image=UIImage(named: "backbutton")
        let label = UILabel(frame: CGRectMake(12, 32, 39, 29))
        label.textAlignment = NSTextAlignment.Center
        label.layer.cornerRadius=4
        label.layer.masksToBounds=true
       // label.text = "BACK"
        label.textColor=UIColor.blueColor()
        label.backgroundColor=UIColor(red: 255/255, green: 55/255, blue: 54/255, alpha: 0.8)
        self.view.addSubview(label)
        self.view.addSubview(imageView)
    
        let tap = UITapGestureRecognizer(target: self, action: Selector("tapFunction:"))
        label.addGestureRecognizer(tap)
        imageView.addGestureRecognizer(tap)
        label.userInteractionEnabled=true
        imageView.userInteractionEnabled=true
        
        imageView.hidden=true
        label.hidden=true
      /*
        for var l=0;l < namearray.count;l++
        {
            
            let imgofhotel=UIImage(named: "\(namearray[l])")
            imagearray1.addObject(imgofhotel!)
            
        }
        
         print(self.imagearray1.count)*/
 //       print(namearray)
        print(sorteddistance1)
        for(var j=0;j < sorteddistance1.count;j++)
        {
            let meters=sorteddistance1[j] as! Double
            
            let kilometers = meters / 1000.0
            
            let miles = kilometers * 0.62137
            
            var string1=String(miles)
            
            string1 = string1.substringToIndex(string1.startIndex.advancedBy(3))
            
            sorteddistance2.addObject(string1)
            
        }
//        for(var i=0;i < 15;i++)
//        {
//        
//           self.json(namearray[i] as! NSString)
//            
//        }
       
        
        
        
        
        
        
        //DEMO BUTTON OF FIRST VIEW DATA
        
        
        
        let path: NSString = NSBundle.mainBundle().pathForResource("new doc withFaceBookID", ofType: "json")!
        let data : NSData = try! NSData(contentsOfFile: path as String, options: NSDataReadingOptions.DataReadingMapped)
        
        let array: NSArray!=(try! NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers)) as! NSArray
        
           print(array.count)
        for var i=0;i < array.count;i++
        {
            
            nameofhotels.addObject(array[i].valueForKey("Name") as! String)
            latitudeofhotels.addObject(array[i].valueForKey("Latitude") as! Double)
            longitudeofhotels.addObject(array[i].valueForKey("Longitude") as! Double)
            
        }
        print(array)
        print(nameofhotels.count)
        
        //INSTAGRAM IDS ARE STORED TO DICTIONARY WITH HOTEL NAMES AS KEY
        
        
        
        for var j = 0;j<self.nameofhotels.count;j++
        {
            
            let nullelement1 = "null"
            let nullelement2 = Double(nullelement1)
            if(array[j].valueForKey("InstagramUserId") as? Double != nullelement2)
            {
                
                dictionaryofinstagramid.setObject(array[j].valueForKey("InstagramUserId")!, forKey: nameofhotels[j] as! String)
                
            }
            
            
        }
      //  print(dictionaryofinstagramid.count)
        for var k = 0;k < nameofhotels.count;k++
        {
            
            if(array[k].valueForKey("TwitterUserName") as! String != "")
            {
                
                dictionaryoftwitterusername.setObject(array[k].valueForKey("TwitterUserName")!, forKey: nameofhotels[k] as! String)
                
            }
            
        }
        
       // print(dictionaryoftwitterusername.count)
        for var l = 0;l < nameofhotels.count;l++
        {
            let nullelement1 = "null"
            let nullelement2 = Double(nullelement1)
            
            if(array[l].valueForKey("FacebookUserId") as? Double != nullelement2)
            {
                
                dictionaryoffacebookid.setObject(array[l].valueForKey("FacebookUserId")!, forKey: nameofhotels[l] as! String)
                
            }
            
        }
        print(dictionaryoffacebookid.count)
        print(dictionaryoftwitterusername.count)
        print(dictionaryofinstagramid.count)
        for var i=0;i<self.nameofhotels.count;i++
        {
            
            let lati = Double(self.latitudeofhotels[i] as! NSNumber)
            let long = Double(self.longitudeofhotels[i] as! NSNumber)
            let destinationlocation=CLLocation(latitude:lati , longitude:long)
            
            //let staticloca=CLLocation(latitude: latitude1, longitude: longitude1)
            //print(staticloca)
            let staticloca=CLLocation(latitude: 34.050977, longitude: -118.248027)
            
            let distance=staticloca.distanceFromLocation(destinationlocation)
            
            distanceofhotels.addObject(distance)
            
            
        }
        
      print(distanceofhotels.count)
        
        //        print(nameofhotels)
        //        print(latitudeofhotels.count)
        //        print(longitudeofhotels.count)
        //        print(distanceofhotels)
        
        dictionaryofname.setObject("123", forKey: "dfgd")
        dictionaryofname.setObject("78338", forKey: "bbff")
        dictionaryofname.setObject("43", forKey: "cjkf")
        dictionaryofname.setObject("34", forKey: "ahjaj")
        print(dictionaryofname)
        
        if((dictionaryofname.objectForKey("ahjaj")) != nil)
        {
            print("exist")
        }
        else
        {
            print("not exist")
        }
        //
        var objectsArray1 = ["\(nameofhotels[0])"]
        for var i=1;i < nameofhotels.count;i++
        {
            
            objectsArray1.append(nameofhotels[i] as! String)
            
        }
        //           print(objectsArray)
        //            print(distancearray)
        var keysArray1 = [distanceofhotels[0]]
        for var j=1;j < distanceofhotels.count;j++
        {
            
            keysArray1.append(distanceofhotels[j])
            
        }
        
        var response1 = Dictionary<String, Double>()
        
        print(keysArray1.count)
        print(objectsArray1.count)
        response1=NSDictionary(objects: keysArray1, forKeys: objectsArray1) as! Dictionary<String, Double>
        print(response1.count)
        let byValue = {
            (elem1:(key: String, val: Double), elem2:(key: String, val: Double))->Bool in
            if elem1.val < elem2.val {
                return true
            } else {
                return false
            }
        }
        let sortedDict = response1.sort(byValue)
        sorteddictarray = response1
        
        print(response1.count)
        print(sorteddictarray.count)
        print(sortedDict)
        
        for var m=0;m<sortedDict.count;m++
        {
            
            nameofhotelssorted.addObject(sortedDict[m].0)
            
        }
        
//        for i in sortedDict
//        {
//            let gggggg = i as! AnyObject
//            sorteddictarray.addObject(i)
//        }
        
        print(nameofhotelssorted)
        let firstelement = sortedDict[0]
        print(firstelement.0)

        
        
        //END OF DEMO BUTTON DATA
        
        print(hotelnamearr)
        
        
       // check()
       
        check1()
    }
    func check1()
    {
       // if(position1<nameofhotelssorted.count)
        if(position1<countofhotel)
        {
       
                        let name = nameofhotelssorted[position1]
            
                        if((dictionaryofinstagramid.objectForKey(name)) != nil && instagramnum == 1)
                        {
                            
                            for i in sorteddictarray
                            {
                                if((i.0) == name as! String)
                                {
                                    let dis1 = i.1 as Double
                                    //let dis2 = String(dis1)
                                    let kilometers = dis1 / 1000.0
                                    
                                    let miles = kilometers * 0.62137
                                    
                                    var string1=String(miles)
                                    if(miles<10)
                                    {
                                        string1 = string1.substringToIndex(string1.startIndex.advancedBy(3))
                                    }
                                    else if(miles>=10 && miles<100)
                                    {
                                    string1 = string1.substringToIndex(string1.startIndex.advancedBy(4))
                                    }
                                    else
                                    {
                                    string1 = string1.substringToIndex(string1.startIndex.advancedBy(5))
                                    }
                                    
                                    distancetoshowongrid.addObject(string1)
                                }
                                
                            }
                            
                            hotelnamearr.addObject(name)
                            
                            let instaid1 = (dictionaryofinstagramid.objectForKey(name)) as! NSNumber
                            let instaid2 = String(instaid1)
                            json(instaid2)
                            
                        }
            
                      else if((dictionaryoftwitterusername.objectForKey(name)) != nil && twitternum == 1)
                         {
                            
                            for i in sorteddictarray
                            {
                                if((i.0) == name as! String)
                                {
                                    let dis1 = i.1 as Double
                                    //let dis2 = String(dis1)
                                    let kilometers = dis1 / 1000.0
                                    
                                    let miles = kilometers * 0.62137
                                    
                                    var string1=String(miles)
                                    
                                    if(miles<10)
                                    {
                                        string1 = string1.substringToIndex(string1.startIndex.advancedBy(3))
                                    }
                                    else if(miles>=10 && miles<100)
                                    {
                                        string1 = string1.substringToIndex(string1.startIndex.advancedBy(4))
                                    }
                                    else
                                    {
                                        string1 = string1.substringToIndex(string1.startIndex.advancedBy(5))
                                    }

                                    distancetoshowongrid.addObject(string1)
                                }
                                
                            }
                            
                            hotelnamearr.addObject(name)
                            
                           let twittername1 = (dictionaryoftwitterusername.objectForKey(name)) as! String
                      
                            // let twittername2 = String(twittername1)
                           twitterimage(twittername1,facebookid: "")
                            
                          }
            
                        else if((dictionaryoffacebookid.objectForKey(name)) != nil && facebooknum == 1)
                        {
                            for i in sorteddictarray
                            {
                                if((i.0) == name as! String)
                                {
                                    let dis1 = i.1 as Double
                                    //let dis2 = String(dis1)
                                    
                                    let kilometers = dis1 / 1000.0
                                    
                                    let miles = kilometers * 0.62137
                                    
                                    var string1=String(miles)
                                    
                                    if(miles<10)
                                    {
                                        string1 = string1.substringToIndex(string1.startIndex.advancedBy(3))
                                    }
                                    else if(miles>=10 && miles<100)
                                    {
                                        string1 = string1.substringToIndex(string1.startIndex.advancedBy(4))
                                    }
                                    else
                                    {
                                        string1 = string1.substringToIndex(string1.startIndex.advancedBy(5))
                                    }

                                    distancetoshowongrid.addObject(string1)
                                }
                                
                            }
                            
                            hotelnamearr.addObject(name)
                            
                            let facebookname1 = (dictionaryoffacebookid.objectForKey(name)) as! NSNumber
                            let facebookname2 = String(facebookname1)
                            twitterimage("",facebookid: facebookname2)
                        }
                        else
                        {
                            print("else run")
                            print(position1)
                            
                            position1++
                            self.check1()
                          }

            
        }
         else
        {
            
            print(position1)
            
            if (UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                countofhotel = countofhotel + 20
            }
            else
            {
            countofhotel = countofhotel + 10
            }
            print(countofhotel)
            countoffooter=1
        }
    
    }
    func check()
    {
        if (UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad)
        {
            
            lastindex1=28
            
            if(position<lastindex1)
            {
                
                self.json(namearray[position] as! NSString)
                
            }
            
            
        }
        else
        {
            
           if(position<lastindex1)
            {
                
              self.json(namearray[position] as! NSString)
              
             }
           else
            {
                footerhidden1=1
                lastindex1=28
                print(lastindex1)
                print(position)
           }
            
            
        }
        
    }
    
    override func didReceiveMemoryWarning()
    {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    

//    override func prefersStatusBarHidden() -> Bool
//    {
//        return true
//    }
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        
        return imagearray1.count
        
    }
    
    // make a cell for each cell index path
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("gridcell", forIndexPath: indexPath) as! CollectionViewCell
    
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
       
        cell.image_view.image=imagearray1[indexPath.row] as? UIImage
        cell.namelabel.text=hotelnamearr[indexPath.row] as? String
        let hoteldistance = hotelnamearr[indexPath.row] as! String
       // print(hoteldistance)
        
//        for i in sorteddictarray
//        {
//            if((i.0) == hoteldistance)
//            {
//                let distancetext1 = i.1 as Double
//                let distancetext2 = String(distancetext1)
//                cell.distancelabel.text = distancetext2
//            }
//        }
        let distancestring = distancetoshowongrid[indexPath.row] as! String
        cell.distancelabel.text = "\(distancestring) Miles"
       
        if (UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad)
        {
            
            
            // voltarbutlabel.titleLabel?.font=voltarbutlabel.titleLabel?.font.fontWithSize(24)
            cell.distancelabel.font=cell.distancelabel.font.fontWithSize(13)
            cell.namelabel.font = cell.namelabel.font.fontWithSize(13)
            
            
        }
        
        
        return cell
        
    }
    
    func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath)
    {
        
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
    
        let deviceSize = UIScreen.mainScreen().bounds.size
       
        //let cellSize = sqrt(Double(deviceSize.width * deviceSize.height) / (Double(33)))
        
        var cellWidth = CGFloat()
        
        var cellHeight = CGFloat()
        
        if (UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad)
        {
            
            cellWidth = ((deviceSize.width / 5) - 6)
            // let cellHeight = (deviceSize.height / 4)
            cellHeight = cellWidth
            

        }
        else
        {
            
            cellWidth = ((deviceSize.width / 3) - 3)
            // let cellHeight = (deviceSize.height / 4)
            cellHeight=cellWidth
            
        }
       
        return CGSize(width: cellWidth , height: cellHeight)
        
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    {
        let cellof = collection_view.cellForItemAtIndexPath(indexPath) as! CollectionViewCell
    //cellof.namelabel.text
        let nameofstring = cellof.namelabel.text
        let obj = self.storyboard!.instantiateViewControllerWithIdentifier("feedvc") as! FeedVC
        //obj.restaurantlabelname=(hotelnamearr[indexPath.row] as? String)!
        obj.restaurantlabelname=nameofstring!
        obj.instagramiddict = dictionaryofinstagramid
        obj.facebookuserdict = dictionaryoffacebookid
        obj.twitteruserdict = dictionaryoftwitterusername
        
       // obj.instagramid=self.instagramid[indexPath.row] as! String
       self.navigationController?.pushViewController(obj, animated: true)
       //https://i.diawi.com/HsVFRH
        
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize
    {
        if((footerhidden1==1)&&(footerhidden2==1))
        {
        return CGSizeMake(self.view.frame.size.width, 0)
        }
        if (UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad)
        {
            return CGSizeMake(self.view.frame.size.width, 100)
        }
        return CGSizeMake(self.view.frame.size.width, 70)
    }
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView
    {
        
        if kind == UICollectionElementKindSectionHeader {
            let header = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: "header", forIndexPath: indexPath)
            
            return header
            
        } else {
            
            let footer = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: "footer1", forIndexPath: indexPath)
            
              if(countoffooter == 0)
              {
                
                footer.hidden=true
                
               }
            else
              {
            
            footer.hidden = false
            
            
            }
//            if(footerhidden1==0&&footerhidden2==0)
//            {
//                footer.hidden=true
//            }
//            else if(footerhidden1==1)
//            {
//                footer.hidden=false
//            }
//            else if(footerhidden1==0&&footerhidden2==1)
//            {
//            footer.hidden=true
//            }
//            else if((footerhidden1==1)&&(footerhidden2==1))
//            {
//            footer.hidden=true
//            }
            
            return footer
        }
    }

    func tapFunction(sender:UITapGestureRecognizer)
    {
        
        let loginManager: FBSDKLoginManager = FBSDKLoginManager()
        loginManager.logOut()
        
        FHSTwitterEngine .sharedEngine() .clearAccessToken()
        
        self.navigationController?.popToRootViewControllerAnimated(true)
        
    }
    
    func json(userid:NSString)
    {
        
        print(userid)
        
        let jsonUrlPath = "https://api.instagram.com/v1/users/\(userid)/?access_token=1639752400.42a1a72.fb181f44b07645ad841ce8da0ea45196"
        let url:NSURL = NSURL(string: jsonUrlPath)!
        
        let session  = NSURLSession .sharedSession()
        
        let task = session.dataTaskWithURL(url, completionHandler: {data,response,error -> Void in
            if error != nil
            {
                print(error!.localizedDescription)
            }
            
            var err:NSError?
           
            do
            {
                self.itemsdict = try NSJSONSerialization.JSONObjectWithData(data! , options: NSJSONReadingOptions.MutableContainers) as! NSMutableDictionary
               
                print(self.itemsdict)
                
                self.instaimgurl=(self.itemsdict.valueForKey("data")?.valueForKey("profile_picture"))! as! NSString
                
                self.hotelname=(self.itemsdict.valueForKey("data")?.valueForKey("full_name"))! as! NSString
                
                let url1 = NSURL(string: self.instaimgurl as String)
                
                let img=NSData(contentsOfURL: url1!)
                
                var imageof = UIImage()
                
                if(img == nil)
                {
                    imageof = UIImage(named: "NoImage")!
                }
                else
                {
                    imageof = UIImage(data: img!)!
                }

                
                self.imagearray1.addObject(imageof)
             //   self.hotelnamearr.addObject(self.hotelname)
                self.instagramid.addObject(userid)
                
                self.position++
                self.lastindex2++
               // self.check()
                
                let name1 = self.nameofhotelssorted[self.position1]
                
                if((self.dictionaryoftwitterusername.objectForKey(name1)) != nil)
                {
                     self.twitternum=1
                     self.instagramnum=0
                     self.facebooknum=0
                     self.check1()
                }
                else
                {
                
                    if((self.dictionaryoffacebookid.objectForKey(name1)) != nil)
                    {
                    
                        self.facebooknum=1
                        self.instagramnum=0
                        self.twitternum=0
                        self.check1()
                    
                    }
                    else
                    {
                        self.position1++
                        self.instagramnum=1
                        self.twitternum=1
                        self.facebooknum=1
                        self.check1()
                    }
                }
                
            } catch _
            {
                print("error ")
            }


            if err != nil
            {
                
                print("Json Error\(err!.localizedDescription)")
                
            }

        

           dispatch_async(dispatch_get_main_queue(),{
            
            self.collection_view.reloadData()
            
           })
            
        })
        
        task.resume()

}
    
    @IBAction func loadmorebutton(sender: AnyObject)
    {
        
//        check()
//        footerhidden1=0
//        footerhidden2=1
        
        self.instagramnum=1
        self.twitternum=1
        self.facebooknum=1
        
        countoffooter = 0
        
        self.collection_view.reloadData()
        
        check1()
        
    }
    
    func twitterimage(username: String,facebookid: String)
    {
        print(username)
        print(facebookid)
//        let jsonUrlPath = "https://twitter.com/\(username)/profile_image?size=original"
//        let url:NSURL = NSURL(string: jsonUrlPath)!
//        
//        
//        let img = NSData(contentsOfURL: url)
//        
//        let imageof = UIImage(data: img!)
//        
//        
//        //self.demofortwitter.image = imageof
//        //self.imageofimsta = imageof!
//        self.imagearray1.addObject(imageof!)
//        
//        self.collection_view.reloadData()
        var image_url = NSURL()
        if(facebookid == "")
        {
           image_url = NSURL(string: "https://twitter.com/\(username)/profile_image?size=original")!
        }
        if(username == "")
        {
           image_url = NSURL(string: "https://graph.facebook.com/\(facebookid)/picture?type=large")!
        }
        
        
        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            // do some task
            
            var image = UIImage()
            
            let image_data = NSData(contentsOfURL: image_url)
            
            
            if(image_data == nil)
            {
                image = UIImage(named: "NoImage")!
            }
            else
            {
            image = UIImage(data: image_data!)!
            }
            
            self.imagearray1.addObject(image)
            
            let name1 = self.nameofhotelssorted[self.position1]
            
            if(facebookid == "")
            {
                if((self.dictionaryoffacebookid.objectForKey(name1)) != nil)
                {
                    self.instagramnum=0
                    self.twitternum=0
                    self.facebooknum=1
                    self.check1()
                }
                else
                {
                self.instagramnum=1
                    self.twitternum=1
                    self.facebooknum=1
                    self.position1++
                    self.check1()
                }
            }
            else if(username == "")
            {
                self.instagramnum=1
                self.twitternum=1
                self.facebooknum=1
                self.position1++
                
                self.check1()
            }
            dispatch_async(dispatch_get_main_queue()) {
                // update some UI
                
                self.collection_view.reloadData()
                
            }
        }
    }

    @IBAction func demobutton(sender: AnyObject)
    {
        print(self.accesstoken)
//        let mediaid = "1354664253143371016_203260730"
        
      //  let mediaid = "1349843781859590630_1507971356"
        let jsonUrlPath = "https://api.instagram.com/v1/users/984451739/media/recent/?access_token=\(self.accesstoken)"
        
        let url:NSURL = NSURL(string: jsonUrlPath)!
        
        let session  = NSURLSession .sharedSession()
        
        let task = session.dataTaskWithURL(url, completionHandler: {data,response,error -> Void in
            if error != nil
            {
                print(error!.localizedDescription)
            }
            
            var err:NSError?
            
            do
            {
                let respnse = try NSJSONSerialization.JSONObjectWithData(data! , options: NSJSONReadingOptions.MutableContainers) as! NSMutableDictionary
                print(respnse)
        
                let jsonUrlPath = "https://api.instagram.com/v1/media/\(respnse["data"]![0]["id"])/comments"
                
                let url:NSURL = NSURL(string: jsonUrlPath)!
                let urlReq:NSMutableURLRequest = NSMutableURLRequest.init(URL: url)
                urlReq.HTTPMethod = "POST"
        
                let bodyData = "access_token=\(self.accesstoken)"
                urlReq.HTTPBody = bodyData.dataUsingEncoding(NSUTF8StringEncoding);

                
                let session  = NSURLSession .sharedSession()
                
                let task1 = session.dataTaskWithRequest(urlReq, completionHandler: { (data, response, error) -> Void in
                    if error != nil
                    {
                        print(error!.localizedDescription)
                    }
                    
                    var err:NSError?
                    
                    do
                    {
                        let respnse = try NSJSONSerialization.JSONObjectWithData(data! , options: NSJSONReadingOptions.MutableContainers) as! NSMutableDictionary
                        print(respnse)
                    }
                    catch _
                    {
                        print("error ")
                    }
                    
                    
                    if err != nil
                    {
                        
                        print("Json Error\(err!.localizedDescription)")
                        
                    }
                    
                    
                    
                    dispatch_async(dispatch_get_main_queue(),{
                        
                        
                        
                    })
                    
                })
                
                task1.resume()
                
                return

            }
            catch _
            {
                print("error ")
            }
            
            
            if err != nil
            {
                
                print("Json Error\(err!.localizedDescription)")
                
            }
            
            
            
            dispatch_async(dispatch_get_main_queue(),{
                
               
                
            })
            
        })
        
        task.resume()
        
    //    https://api.instagram.com/v1/media/{media-id}/comments and sending parameter text=testing..
//        let result = FHSTwitterEngine.sharedEngine().getTimelineForUser("@BottegaLouie", isID: false, count: 10)
////                print(result[0].valueForKey("favorite_count"))
////                print(result[0].valueForKey("retweet_count"))
////                print(result[0].valueForKey("text"))
////                print(result[0].valueForKey("extended_entities")?.valueForKey("media")?.valueForKey("media_url_https"))
//              print(result)
//        print(FHSTwitterEngine.sharedEngine().accessToken.key)
     }
}
