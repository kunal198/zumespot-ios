//
//  tabledemo.swift
//  ZumeSpot
//
//  Created by mrinal khullar on 8/26/16.
//  Copyright © 2016 mrinal khullar. All rights reserved.
//

import UIKit

class tabledemo: UIViewController, UITableViewDelegate,UITableViewDataSource, UIScrollViewDelegate
{

    var heightof1=CGFloat()
    var heightof2=CGFloat()
    
    var loadmoresection=1
    
    @IBOutlet weak var tableleft: UITableView!
    @IBOutlet weak var tableright: UITableView!
    @IBOutlet weak var backbutton: UIButton!
    
    var extraarray:[CGFloat]=[]
    var extraarray2:[CGFloat]=[]
    
    var imagearray:[UIImage]=[]
    var imagearray2:[UIImage]=[]
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        backbutton.layer.cornerRadius=5
        
        self.tableleft .showsVerticalScrollIndicator=false
        self.tableright.showsVerticalScrollIndicator=false
        self.tableleft.allowsSelection=false
        self.tableright.allowsSelection=false
        self.tableleft.backgroundView=UIImageView(image: UIImage(named: "gridbackonfeed"))
        self.tableright.backgroundView=UIImageView(image: UIImage(named: "gridbackonfeed"))
       // UIImageView(image: UIImage(named: "taylor-swift"))
        
        imagearray=[UIImage(named: "Black Hogg")!,UIImage(named: "Covell")!,UIImage(named: "Far Bar LA")!,UIImage(named: "Guelaguetza")!,UIImage(named: "Black Hogg")!,UIImage(named: "Covell")!]
        imagearray2=[UIImage(named: "Covell")!,UIImage(named: "Far Bar LA")!,UIImage(named: "Guelaguetza")!,UIImage(named: "Covell")!,UIImage(named: "Far Bar LA")!,UIImage(named: "Black Hogg")!]
         extraarray=[225.0,250.0,240.0,220.0,240.0,200.0]
        extraarray2=[125.0,155.0,200.0,125.0,180.0,200.0]
       // self.tableleft .separatorColor=UIColor.clearColor()
        self.tableleft.separatorStyle=UITableViewCellSeparatorStyle.None
        
    }

    override func didReceiveMemoryWarning()
    {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
     return 2
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
       if(section==0)
       {
          return imagearray.count
        }
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        if(tableView == self.tableleft)
        {
            var cell=tabledemoccell()
            cell=tableView.dequeueReusableCellWithIdentifier("cell1") as! tabledemoccell
            cell.backgroundColor=UIColor.greenColor()
            
            cell.imageview.image=imagearray[indexPath.row]
            return cell
        }
        else
        {
            
            var cell1=tabledemocell2()
            cell1=tableView.dequeueReusableCellWithIdentifier("cell2") as! tabledemocell2
            cell1.backgroundColor=UIColor.redColor()
            cell1.image_view.image=imagearray2[indexPath.row]
            return cell1
            
        }
        
        
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        
        var heightofcell=CGFloat()
        if(tableView==self.tableleft)
        {
           
        heightofcell=extraarray[indexPath.row]
            heightof1=heightof1 + heightofcell
        }
        else
        {
        heightofcell=extraarray2[indexPath.row]
            heightof2=heightof2 + heightofcell
        }
        return heightofcell
        
    }
    
    func tableView(tableView: UITableView,
        willDisplayCell cell: UITableViewCell,
        forRowAtIndexPath indexPath: NSIndexPath)
    {
        
        cell.backgroundColor=UIColor.clearColor()
       
//        let additionalSeparatorThickness = CGFloat(5)
//        let additionalSeparator = UIView(frame: CGRectMake(0,
//            cell.frame.size.height - additionalSeparatorThickness,
//            cell.frame.size.width,
//            additionalSeparatorThickness))
//        additionalSeparator.backgroundColor = UIColor.clearColor()
//        additionalSeparator.alpha=0.1
//        cell.addSubview(additionalSeparator)
        
    }
    
    
       func scrollViewDidScroll(scrollView: UIScrollView)
    {

        var slaveTable: UITableView? = nil
        let maxSize = CGSizeMake(max(tableleft.contentSize.width, tableright.contentSize.width), max(tableleft.contentSize.height, tableright.contentSize.height))
        
        scrollView.contentSize.height=maxSize.height
    
        
        if self.tableleft == scrollView
            
        {
            slaveTable = self.tableright
        }
            
        else if self.tableright == scrollView
            
        {
            slaveTable = self.tableleft
        }
        
        let height: CGFloat = scrollView.frame.size.height
        let contentYoffset: CGFloat = scrollView.contentOffset.y
        let distanceFromBottom: CGFloat = scrollView.contentSize.height - contentYoffset
        if distanceFromBottom < height
        {
            
            print(heightof1)
            print(heightof2)
            
        }
        
        slaveTable!.contentOffset = scrollView.contentOffset
        
    }
   /* func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        
        let view1=UIView()
        
        if(tableView==self.tableleft)
        {
            
        let frame = tableView.frame
        let addButton = UIButton(frame: CGRectMake(frame.size.width-60, 25, 100, 30))
        addButton.setTitle("Load More", forState: .Normal)
        addButton.setTitleColor(UIColor.redColor(), forState: .Normal)
        addButton.backgroundColor = UIColor.yellowColor()
        
        let headerView = UIView(frame: CGRectMake(0, 0, frame.size.width, frame.size.height))
        
        headerView.addSubview(addButton)
        return headerView
            
        }
        
        return view1
        
    }*/
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if(section==self.loadmoresection && tableView==self.tableleft)
        {
         return 3.0
        }
        return 0
    }
    @IBAction func backbutton(sender: AnyObject)
    {
        
        self.navigationController?.popViewControllerAnimated(true)
        
    }
}
