//
//  FeedVC.swift
//  ZumeSpot
//
//  Created by mrinal khullar on 8/23/16.
//  Copyright © 2016 mrinal khullar. All rights reserved.
//

import UIKit


class FeedVC: UIViewController,UICollectionViewDataSource, UICollectionViewDelegate, UITextViewDelegate,CHTCollectionViewDelegateWaterfallLayout
{

    let HEADER_IDENTIFIER = "WaterfallHeader"
    let FOOTER_IDENTIFIER = "WaterfallFooter"
    
    var heightoffooter = CGFloat()
    
    
    @IBOutlet weak var backbutton: UIButton!
    
    @IBOutlet var hotelnamelabel: UILabel!
    @IBOutlet weak var collection_view: UICollectionView!
    
    @IBOutlet var activityindicator: UIActivityIndicatorView!
    @IBOutlet var loadinglabel: UILabel!
    @IBOutlet weak var postbutton: UIButton!
    @IBOutlet weak var posttextview: UITextView!
    @IBOutlet var systemdatelabel: UILabel!
   
    @IBOutlet var zumespoticontop: UIImageView!
    @IBOutlet weak var loading_indicator: UIActivityIndicatorView!
   
    var placeholderstring="Write Comment"
    
    var restaurantlabelname=NSString()
    var instagramid=String()
    
    var imagearray:[UIImage]=[]
    var extraarray:[CGFloat]=[]
    var itemsdict = NSMutableDictionary()
    var instaimgurl=NSArray()
    var instagramimgarr=NSMutableArray()
    var hotelname=NSString()
    var imagearray1=NSMutableArray()
    var instacommentsarr=NSMutableArray()
    var instalikesarr=NSMutableArray()
    var instacaptionarr=NSMutableArray()
    var instadatearray1=NSArray()
    var instadatearray2=NSMutableArray()
    
    var imagearray3=NSMutableArray()
    
    var footerhidden=0
    //[10.0,30.0,10.0,10.0,30.0,10.0]
    //
    var sizearray1=[10.0,200.0,280.0,280.0,172.0,180.0,90.0,100.0,10.0,200.0,280.0,280.0,172.0,180.0,90.0,100.0,172.0,180.0,90.0,100.0]
    
  //  var sizearray2=[(self.view.frame.width/2)-3,220.0,300.0,300.0,192.0,200.0,110.0,120.0,25.0,220.0,300.0,300.0,192.0,200.0,110.0,120.0,192.0,200.0,110.0,120.0]
    var sizearray3=NSMutableArray()
    
    
    let obj1 = ViewController()
    
    let label = UILabel()
    
    var backviewofactivity = UIView()
    
    var facebookidresponse = NSMutableDictionary()
    
    var facebookimageres = NSMutableDictionary()
    
    var facebookimageurlarr = NSMutableArray()
    
    var facebooklikecountdict = NSMutableDictionary()
    
    var facebooklikecountarray = NSMutableArray()
    
    var facebookcommentcountdict = NSMutableDictionary()
    
    var facebookcommentcountarray = NSMutableArray()
    
    var insfbtwittercount1 = 0
    var insfbtwittercount2 = 0
    var facebookurlcount = 0
    
    var facebookimageurlcount = 0
    
    var facebookidarray = NSMutableArray()
    var facebookdatearray = NSMutableArray()
    var facebookmessagearray = NSMutableArray()
    //TWITTER POSTVARIABLES
    
    var twitterimagesarr = NSMutableArray()
    
    var twittercount = 0
    
    var instagramiddict = NSMutableDictionary()
    
    var facebookuserdict = NSMutableDictionary()
    
    var twitteruserdict = NSMutableDictionary()
    
     var facebookimageurl = NSMutableArray()
    
    var facebookandtwitter = 0
    
    var posttextviewcount = 0
    
    var heightofcellarray = NSMutableArray()
    
    var indexofselectedrow = 100
    
    var idsofposts = NSMutableArray()
    
    var messagetopost = String()
    
    var instafbtwitter = NSMutableArray()
    
       override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        print(restaurantlabelname)
        print(twitteruserdict.count)
        print(facebookuserdict)
        print(instagramiddict.count)
        //BACK VIEW OF ACTIVITY INDICATOR
        let widthofview = self.view.frame.size.width
        let heightofview = self.view.frame.size.height
        
        let yofbackview = self.collection_view.frame.origin.y
        
        backviewofactivity.frame = CGRectMake(0, yofbackview, widthofview, heightofview-yofbackview)
        backviewofactivity.backgroundColor = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 0.5)
        self.view.addSubview(backviewofactivity)
        self.view.addSubview(loading_indicator)
        self.view.addSubview(loadinglabel)
        
        
        //DATE OF LABEL
        let todaysDate:NSDate = NSDate()
        let dateFormatter:NSDateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd MMMM yyyy"
        let DateInFormat:String = dateFormatter.stringFromDate(todaysDate)
        print(DateInFormat)

        self.systemdatelabel.text = DateInFormat
        if (UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad)
        {
            heightoffooter = 80
        }
        else
        {
        heightoffooter = 70
        }
        
        
        if (UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad)
        {
            
            hotelnamelabel.font = hotelnamelabel.font.fontWithSize(29)
            systemdatelabel.font = systemdatelabel.font.fontWithSize(20)
            
            postbutton.titleLabel?.font = postbutton.titleLabel?.font.fontWithSize(20)
            
            posttextview.font = posttextview.font!.fontWithSize(21)
            
            backbutton.frame.size = CGSizeMake(60, 60)
            
            
            zumespoticontop.frame.size.width = zumespoticontop.frame.width-10
            
            zumespoticontop.frame.size.height = zumespoticontop.frame.height+6
            
            zumespoticontop.frame.origin.y = zumespoticontop.frame.origin.y-4
           zumespoticontop.frame.origin.x = zumespoticontop.frame.origin.x+7
            
        }
        
        
        label.frame = CGRectMake(14, 15, ((self.view.frame.width/2)-6-20), 15)
        label.textAlignment = NSTextAlignment.Natural
        label.layer.cornerRadius=4
        label.textColor=UIColor.blueColor()
        if (UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad)
        {
        label.font = UIFont.systemFontOfSize(20)
        
        }
        else
        {
        label.font = UIFont.systemFontOfSize(12)
        }
        
        label.text = "Label"
        
        label.backgroundColor=UIColor(red: 255/255, green: 55/255, blue: 54/255, alpha: 0.8)
        self.view.addSubview(label)
        label.hidden = true
        
        let obj="jkdsfjkds"

        
        hotelnamelabel.text=self.restaurantlabelname as String
        self.posttextview.text=self.placeholderstring
        
        print(self.instagramid)
        let sizearray2=[(self.view.frame.width/2)+50,(self.view.frame.width/2)+20,(self.view.frame.width/2)+15,(self.view.frame.width/2)+30,(self.view.frame.width/2)+20,(self.view.frame.width/2)+20,(self.view.frame.width/2)+10,(self.view.frame.width/2)+200,(self.view.frame.width/2)+20,(self.view.frame.width/2)+10,(self.view.frame.width/2)+30,(self.view.frame.width/2)+10,(self.view.frame.width/2)+20,(self.view.frame.width/2)+20,(self.view.frame.width/2)+10,(self.view.frame.width/2)+30,(self.view.frame.width/2)+10,(self.view.frame.width/2)+200,(self.view.frame.width/2)+20,(self.view.frame.width/2)+30]
        
        
        
//        for i in sizearray2
//        {
//            
//        sizearray3.addObject(i)
//            
//        }
        
//        let layout = UICollectionViewFlowLayout()
//        layout.minimumInteritemSpacing=1
//        layout.minimumLineSpacing=4
//        layout.footerReferenceSize=CGSize(width: 320, height: 50)
//        collection_view.collectionViewLayout=layout
        
        backbutton.layer.cornerRadius=3
        postbutton.layer.cornerRadius=3
        posttextview.layer.cornerRadius=3
        
        imagearray=[UIImage(named: "Black Hogg")!,UIImage(named: "Covell")!,UIImage(named: "Far Bar LA")!,UIImage(named: "Guelaguetza")!,UIImage(named: "Black Hogg")!,UIImage(named: "Covell")!]
    
     //   self.activityindicator.hidden=false
        self.loadinglabel.hidden=false
        self.loading_indicator.hidden=false
        self.loading_indicator.startAnimating()
        self.backviewofactivity.hidden=false
        self.collection_view.backgroundView?.contentMode = UIViewContentMode.ScaleAspectFit
        self.collection_view.backgroundView = UIImageView(image: UIImage(named: "gridbackonfeed")!)
        
        setupCollectionView()

        extraarray=[100.0,40.0,100.0,60.0,40.0,100.0]
        
      //  self.activityindicator.startAnimating()
        
        self.instagramid="183002827"
        
        self.api()
        
    }
    func api()
    {
        if((instagramiddict.objectForKey(restaurantlabelname)) != nil)
        {
            let instaid1 = (instagramiddict.objectForKey(restaurantlabelname)) as! NSNumber
            let instaid2 = String(instaid1)
            self.json(instaid2)
                    if((facebookuserdict.objectForKey(restaurantlabelname)) == nil && (twitteruserdict.objectForKey(restaurantlabelname)) == nil)
                      {
                           self.footerhidden=0
                
                           heightoffooter=0
                        
                        self.collection_view.reloadData()
                        }
                      else if((facebookuserdict.objectForKey(restaurantlabelname)) != nil)
                          {
                                 self.facebookandtwitter = 0
                            }
                         else  if((twitteruserdict.objectForKey(restaurantlabelname)) != nil)
                          {
            
                                 self.facebookandtwitter = 1
                           }
                          else
                          {
                            self.facebookandtwitter = 0
                            }
        }
        else if((facebookuserdict.objectForKey(restaurantlabelname)) != nil)
        {
            let fbuser1 = facebookuserdict.objectForKey(restaurantlabelname) as! NSNumber
            let fbuser2 = String(fbuser1)
            self.facebookfeeds(fbuser2)
            
            
            
            if((twitteruserdict.objectForKey(restaurantlabelname)) != nil)
            {
            self.facebookandtwitter = 1
            }
            else
            {
                self.footerhidden=0
                
                heightoffooter=0
                
                self.collection_view.reloadData()
            }
            
        }
        else  if((twitteruserdict.objectForKey(restaurantlabelname)) != nil)
        {
            if(FHSTwitterEngine.sharedEngine().accessToken.key == nil)
            {
                let alert = UIAlertController(title: "Alert", message: "If you want to see feeds from Twitter,then authorize the app through login with Twitte", preferredStyle: UIAlertControllerStyle.Alert)
                let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
                    self.loadinglabel.hidden=true
                    self.loading_indicator.hidden=true
                    self.loading_indicator.stopAnimating()
                    self.backviewofactivity.hidden=true
                }
                let okAction = UIAlertAction(title: "OK", style: .Default) { (action) in
                    let loginController = FHSTwitterEngine .sharedEngine() .loginControllerWithCompletionHandler { (Bool success) -> Void in
                        
                        print(success)
                        
                        let twittername1 = (self.twitteruserdict.objectForKey(self.restaurantlabelname)) as! String
                        
                        self.gettwitterposts(twittername1)
                        
                        self.loadinglabel.hidden=false
                        self.loading_indicator.hidden=false
                        self.loading_indicator.startAnimating()
                        self.backviewofactivity.hidden=false
                        
                        self.footerhidden=0
                        
                        self.heightoffooter=0
                        
                        self.collection_view.reloadData()
                        
//                        let obj = self.storyboard!.instantiateViewControllerWithIdentifier("gridvc") as! GridVC
//                        obj.namearray=self.namearray
//                        obj.sorteddistance1=self.sorteddistance
//                        self.navigationController?.pushViewController(obj, animated: true)
                        
                        } as UIViewController
                    self .presentViewController(loginController, animated: true, completion: nil)

                }
                alert.addAction(cancelAction)
                alert.addAction(okAction)
                self.presentViewController(alert, animated: true, completion: nil)
            }
            else
            {
            let twittername1 = (twitteruserdict.objectForKey(restaurantlabelname)) as! String
            
            self.gettwitterposts(twittername1)
            
            self.footerhidden=0
            
            heightoffooter=0
            
            self.collection_view.reloadData()
            }
        }
        else
        {
            
        self.loadinglabel.hidden=true
            self.loading_indicator.hidden=true
            self.loading_indicator.stopAnimating()
            self.backviewofactivity.hidden=true
        }

       
    }
    func setupCollectionView()
    {
        
        // Create a waterfall layout
        let layout = CHTCollectionViewWaterfallLayout()
        
        // Change individual layout attributes for the spacing between cells
        
        if (UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad)
        {
            layout.minimumColumnSpacing = 4.0
            layout.minimumInteritemSpacing = 4.0
            layout.sectionInset.left=4.0
            layout.sectionInset.right=4.0
        }
        else
        {
            layout.minimumColumnSpacing = 4.0
            layout.minimumInteritemSpacing = 4.0
            layout.sectionInset.left=4.0
            layout.sectionInset.right=4.0
        }
        
        
        // Collection view attributes
        self.collection_view.autoresizingMask = [UIViewAutoresizing.FlexibleHeight, UIViewAutoresizing.FlexibleWidth]
        self.collection_view.alwaysBounceVertical = true
        
        // Add the waterfall layout to your collection view
        self.collection_view.collectionViewLayout = layout
        
        collection_view.registerClass(collection_reusableviewCollectionReusableView.self, forSupplementaryViewOfKind: CHTCollectionElementKindSectionFooter, withReuseIdentifier: FOOTER_IDENTIFIER)

      //  layout.footerHeight = 50
    
        //obj1.demolabelforfeed.text = "dffdgf"
        
    }

    override func didReceiveMemoryWarning()
    {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.

    }

//    override func prefersStatusBarHidden() -> Bool
//    {
//        
//        return true
//        
//    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        
        return imagearray3.count
        
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! CollectionViewCellFeed1
        
        cell.likebutton.tag = indexPath.row
        cell.commentbutton.tag = indexPath.row
        cell.postbutton.tag = indexPath.row
        cell.image_view.image=imagearray1[indexPath.row] as? UIImage
       // cell.commenttext.text=self.instacaptionarr[indexPath.row].valueForKey("text") as? String
        cell.commenttextview.text = self.instacaptionarr[indexPath.row] as? String
        let commentstr=self.instacommentsarr[indexPath.row] as! Int
        cell.comment_count.text=String(commentstr)
       let likestr=self.instalikesarr[indexPath.row] as! Int
         cell.likecount.text=String(likestr)
        cell.date_label.text=self.instadatearray2[indexPath.row] as? String
        
        cell.postcommenttextview.text = "Write Comment"
        
        cell.setNeedsLayout()
        
        cell.layoutIfNeeded()
        
        cell.layoutSubviews()
        
        
        var num1 = CGFloat()
        
        if (UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad)
        {
            
            num1 = (self.view.frame.width/2)-6
            
        }
        else
        {
        num1 = (self.view.frame.width/2)-6
        }
        
        cell.image_view.frame = CGRectMake(0, 0, num1, num1)
        
        
        cell.view_back.frame.origin = CGPointMake(0, num1+2)
        //let heightfromarray = heightofcellarray[indexPath.row] as! CGFloat
        cell.view_back.frame.size.height = cell.frame.height - cell.image_view.frame.height
        
//        cell.commenttext.frame.origin = CGPointMake(10, 0)
//        cell.commenttext.frame.size.width = cell.frame.width-20
//        cell.commenttext.frame.size.height = sizearray3[indexPath.row] as! CGFloat
        cell.commenttextview.frame.origin = CGPointMake(10, 0)
                cell.commenttextview.frame.size.width = cell.frame.width-20
                cell.commenttextview.frame.size.height = sizearray3[indexPath.row] as! CGFloat
        cell.date_label.frame.origin = CGPointMake(10, (sizearray3[indexPath.row] as! CGFloat)+2)
        
       
         cell.commenttextview.contentOffset.y=0

        if (UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad)
        {
            cell.date_label.frame.size.height = 22
            cell.date_label.font = cell.date_label.font.fontWithSize(19)
            
            cell.likeimage.frame.origin.x = 10
            cell.likeimage.frame.origin.y = cell.date_label.frame.origin.y+23
            cell.likeimage.frame.size = CGSizeMake(19, 22)
            
            
            
            cell.likecount.frame.origin.x = 45
            cell.likecount.frame.origin.y = cell.date_label.frame.origin.y+23
            cell.likecount.frame.size.height = 22
            cell.likecount.font = cell.likecount.font.fontWithSize(19)
            
           // cell.comment_count.frame.origin.x = cell.likecount.frame.origin.x + cell.likecount.frame.width + 10
            cell.commentimage.frame.origin.y = cell.date_label.frame.origin.y + 23
            cell.commentimage.frame.size = CGSizeMake(19, 23)
            cell.commentimage.frame.origin.x = cell.likecount.frame.origin.x + cell.likecount.frame.width + 10
            
            
            cell.comment_count.frame.origin.y = cell.date_label.frame.origin.y+23
            cell.comment_count.frame.size.height = 22
            cell.comment_count.frame.origin.x = cell.commentimage.frame.origin.x + cell.commentimage.frame.size.width + 10
            cell.comment_count.font = cell.comment_count.font.fontWithSize(19)
            
        }
        else
        {
            cell.date_label.frame.size.height = 15
            
            cell.likeimage.frame.origin.y = cell.date_label.frame.origin.y+16
            cell.commentimage.frame.origin.y = cell.date_label.frame.origin.y+16
            cell.likeimage.frame.size = CGSizeMake(14, 16)
            cell.commentimage.frame.size = CGSizeMake(14, 16)
            
            cell.comment_count.frame.origin.y = cell.date_label.frame.origin.y+16
            cell.likecount.frame.origin.y = cell.date_label.frame.origin.y+16
            cell.comment_count.frame.size.height = 15
            cell.likecount.frame.size.height = 15
            
            
        }
        

        if (UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad)
        {
            
           // cell.commenttext.font = cell.commenttext.font.fontWithSize(20)
           cell.commenttextview.font = cell.commenttextview.font!.fontWithSize(20)
            cell.postcommenttextview.font = cell.postcommenttextview.font?.fontWithSize(20)
        }
//        if(posttextviewcount == 0)
//        {
//            cell.postcommenttextview.frame.origin.x = cell.likeimage.frame.origin.x
//            cell.postcommenttextview.frame.origin.y = cell.likeimage.frame.origin.y + cell.likeimage.frame.height + 1
//            
//        }
//        else
//        {
//        
//        }
        if(indexPath.row == indexofselectedrow)
        {
            cell.postcommenttextview.frame.origin.x = cell.likeimage.frame.origin.x
            cell.postcommenttextview.frame.origin.y = cell.likeimage.frame.origin.y + cell.likeimage.frame.height + 1
            cell.postcommenttextview.frame.size.height = 40
            
            cell.postbutton.frame.origin.x = cell.postcommenttextview.frame.origin.x + cell.postcommenttextview.frame.size.width + 2
            
            cell.postbutton.frame.origin.y = cell.postcommenttextview.frame.origin.y + 12
            
            cell.postbutton.frame.size.height = 16
            
        }
        else
        {
            cell.postcommenttextview.frame.origin.x = cell.likeimage.frame.origin.x
            cell.postcommenttextview.frame.origin.y = cell.likeimage.frame.origin.y + cell.likeimage.frame.height + 1
            cell.postcommenttextview.frame.size.height = 0
            
            cell.postbutton.frame.origin.x = cell.postcommenttextview.frame.origin.x + cell.postcommenttextview.frame.size.width + 2
            
            cell.postbutton.frame.origin.y = cell.postcommenttextview.frame.origin.y
            
            cell.postbutton.frame.size.height = 0
        }
        cell.likebutton.frame = cell.likeimage.frame
        cell.commentbutton.frame = cell.commentimage.frame
        return cell
        
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        
     //    create a cell size from the image size, and return the size
        print(self.instacommentsarr.count)
        

        if(self.insfbtwittercount1==0)
        {
            
        for(var i = 0;i < self.instacommentsarr.count;i++)
        {
            
            var xyz = CGFloat()
            
            label.text=self.instacaptionarr[i] as? String
            let labeltext = self.instacaptionarr[i] as? String
            
            if (UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                if(labeltext?.characters.count>100)
                {
                    xyz = 102
                }
                else
                {
                    xyz = self.getLabelHeight(label)+20
                }
                
            }
            else
            {
                if(labeltext?.characters.count>100)
                {
                    xyz = 65
                }
                else
                {
                    xyz = self.getLabelHeight(label)+5
                }
            }
           
            
            
            sizearray3.addObject(xyz)
            
            print(xyz)
            
        }
            self.insfbtwittercount1=1
            
            }
        else
        {
            if(twittercount==1)
          {
            for var abc = 20;abc<30;abc++
            {
            sizearray3.addObject(65)
            }
            
          }
        }
        
        var width3 = CGFloat()
        
        if (UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad)
        {
            
            width3 = (self.view.frame.width/2)-6
            
        }
        else
        {
            
            width3 = (self.view.frame.width/2)-6
            
        }
        
        var height1=CGFloat()
        
        if (UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad)
        {
             height1 = (sizearray3[indexPath.row] as! CGFloat)+width3+60
        }
        else
        {
             if(indexPath.row == indexofselectedrow)
             {
                height1 = (sizearray3[indexPath.row] as! CGFloat)+width3+80
             }
            else
             {
             height1 = (sizearray3[indexPath.row] as! CGFloat)+width3+42
            }
            
        }
        // let height2=CGFloat(height1)
        
        heightofcellarray.addObject(height1)
        
        let imagesize = CGSizeMake(width3, height1)
        
        return imagesize
        
    }
    @IBAction func backbutton(sender: AnyObject)
    {
        //hjhj hfhfhf hshshs aeyewyry euejrk
    self.navigationController?.popViewControllerAnimated(true)
        
        
    }
    
    @IBAction func postaction(sender: UIButton)
    {
        
    }
    func textViewShouldBeginEditing(textView: UITextView) -> Bool
    {
        if(textView.text==self.placeholderstring)
        {
//            let indx = NSIndexPath(index: self.indexofselectedrow)
//            let cell = collection_view.cellForItemAtIndexPath(indx) as! CollectionViewCellFeed1
           // cell.postcommenttextview.text=""
            self.posttextview.text=""
            textView.text = ""
            return true
            
        }
        return true
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool
    {
        if(text==("\n"))
        {
            if(textView.text=="")
            {
//                let index1 = NSIndexPath(index: self.indexofselectedrow)
//                let cell = collection_view.cellForItemAtIndexPath(index1) as! CollectionViewCellFeed1
                textView.text=self.placeholderstring
                
            }
            else
            {
            self.messagetopost = textView.text
            }
            textView.resignFirstResponder()
            return false
        }
        else
        {
         self.messagetopost = textView.text
        }

        return true
    }

    func json(userid:NSString)
    {
        let jsonUrlPath = "https://api.instagram.com/v1/users/\(userid)/media/recent/?access_token=3079201992.e029fea.59a10e100f4f45dbb208ae3ee54e9f94"
        let url:NSURL = NSURL(string: jsonUrlPath)!
        
        let session  = NSURLSession .sharedSession()
        let task = session.dataTaskWithURL(url, completionHandler: {data,response,error -> Void in
            if error != nil
            {
                
                print(error!.localizedDescription)
                
            }
            
            var err:NSError?
            
            do
            {
                
                self.itemsdict = try NSJSONSerialization.JSONObjectWithData(data! , options: NSJSONReadingOptions.MutableContainers) as! NSMutableDictionary
                
            } catch _
            {
                print("error ")
            }
            
            print(self.itemsdict)
            self.instaimgurl=(self.itemsdict.valueForKey("data")?.valueForKey("images"))! as! NSArray
            let instagramcomnt=[self.itemsdict.valueForKey("data")?.valueForKey("comments")][0] as! NSArray
            for var y=0;y<20;y++
            {
                self.instafbtwitter.addObject("instagram")
                self.instacommentsarr.addObject(instagramcomnt[y].valueForKey("count")!)
            }
            // self.instacommentsarr = instagramcomnt.mutableCopy() as! NSMutableArray
            let instalike=[self.itemsdict.valueForKey("data")?.valueForKey("likes")][0] as! NSArray
            for var z=0;z<20;z++
            {
                self.instalikesarr.addObject(instalike[z].valueForKey("count")!)
            }
           // self.instalikesarr=instalike.mutableCopy() as! NSMutableArray
            let instacaption = [self.itemsdict.valueForKey("data")?.valueForKey("caption")][0] as! NSArray
            for var x=0;x<20;x++
            {
            self.instacaptionarr.addObject(instacaption[x].valueForKey("text")!)
                self.idsofposts.addObject("str")
            }
            print(self.instacaptionarr)
            self.instadatearray1=[self.itemsdict.valueForKey("data")?.valueForKey("caption")][0] as! NSArray
            for var a=0;a < self.instaimgurl.count;a++
            {
                
               let createdtime1=self.instadatearray1[a].valueForKey("created_time") as! String
                let createdtime2=Double(createdtime1)
                let date = NSDate(timeIntervalSince1970: createdtime2!)
                //print(date)
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "dd MMM yyyy"
                dateFormatter.timeZone = NSTimeZone(name: "UTC")
                let dateString = dateFormatter.stringFromDate(date)
                self.instadatearray2.addObject(dateString)
                
            }
            print(self.instadatearray2)
            let vvvv=self.instadatearray1[0].valueForKey("created_time")
            print(vvvv)
            print(self.instadatearray1)

            let date = NSDate(timeIntervalSince1970: 1472833920)
            print(date)
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "dd MMM yyyy"
            dateFormatter.timeZone = NSTimeZone(name: "UTC")
            let dateString = dateFormatter.stringFromDate(date)
            print("formatted date is =  \(dateString)")
           
            
            for var i=0;i < self.instaimgurl.count;i++
            {
                
                if (UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad)
                {
                    self.instagramimgarr.addObject((self.instaimgurl[i].valueForKey("standard_resolution")?.valueForKey("url"))!)
                    
                }
                else
                {
                
                    self.instagramimgarr.addObject((self.instaimgurl[i].valueForKey("low_resolution")?.valueForKey("url"))!)
                
                }
                
            }

           for var j=0;j < self.instaimgurl.count;j++
           {
            
            let url = NSURL(string: self.instagramimgarr[j] as! String)
            
            let img=NSData(contentsOfURL: url!)
            
            let imageof = UIImage(data: img!)
            self.imagearray1.addObject(imageof!)

            }
            
            for var x=0;x < self.imagearray1.count;x++
            {
                
                self.imagearray3.addObject(self.imagearray1[x])
                
            }

            
            
            if err != nil
            {
                print("Json Error\(err!.localizedDescription)")
            }
            dispatch_async(dispatch_get_main_queue(),{
                self.collection_view.reloadData()
            
                self.loadinglabel.hidden=true
                
                self.loading_indicator.hidden=true
                self.loading_indicator.stopAnimating()
                self.backviewofactivity.hidden=true
                self.footerhidden=1
                
            })
        })
        
        task.resume()
    
    }
    
    
    func collectionView (collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
        heightForFooterInSection section: NSInteger) -> CGFloat
    {
    
      return heightoffooter
    
    }
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView
    {
        var reusableView: UICollectionReusableView? = nil
        if (kind == CHTCollectionElementKindSectionHeader) {
            reusableView = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: HEADER_IDENTIFIER, forIndexPath: indexPath)
        }
        else if (kind == CHTCollectionElementKindSectionFooter) {
            reusableView = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: FOOTER_IDENTIFIER, forIndexPath: indexPath) as! collection_reusableviewCollectionReusableView
            
            
            if (UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad)
            {
                let centreofview = self.view.frame.size.width / 2
                
                let xoflabel = centreofview - 55
                
                let label = UILabel(frame: CGRectMake(xoflabel, 25, 110, 30))
                label.textAlignment = NSTextAlignment.Center
                label.layer.cornerRadius=3
                label.layer.masksToBounds=true
                label.text = "Load More"
                label.textColor=UIColor.whiteColor()
                label.font = label.font.fontWithSize(16)
                label.backgroundColor=UIColor(red: 255/255, green: 55/255, blue: 54/255, alpha: 0.8)
                reusableView!.addSubview(label)
                
                let tap = UITapGestureRecognizer(target: self, action: Selector("tapFunction:"))
                label.addGestureRecognizer(tap)
                
                label.userInteractionEnabled=true

            }
            else
            {
                let centreofview = self.view.frame.size.width / 2
                
                let xoflabel = centreofview - 47
                
                let label = UILabel(frame: CGRectMake(xoflabel, 22, 94, 25))
                label.textAlignment = NSTextAlignment.Center
                label.layer.cornerRadius=3
                label.layer.masksToBounds=true
                label.text = "Load More"
                label.textColor=UIColor.whiteColor()
                label.font = label.font.fontWithSize(13)
                label.backgroundColor=UIColor(red: 255/255, green: 55/255, blue: 54/255, alpha: 0.8)
                reusableView!.addSubview(label)
                
                let tap = UITapGestureRecognizer(target: self, action: Selector("tapFunction:"))
                label.addGestureRecognizer(tap)
                
                label.userInteractionEnabled=true

            }
            
            if(footerhidden==0)
                        {
            
                        reusableView!.hidden=true
            
                        }
                        else
                        {
            
                        reusableView!.hidden=false
                            
                        }
            

        }
        
        return reusableView!

        
    }
    func hfhfhfh()
    {
      
    }
    @IBAction func todosPush(sender: AnyObject)
    {
        
        print("success")
        for var y=10;y<self.imagearray1.count;y++
        {
        self.imagearray3.addObject(self.imagearray1[y])
        }
        self.collection_view.reloadData()
        
        self.footerhidden=0
        
    }
    
    func getLabelHeight(label: UILabel) -> CGFloat
    {
        
        let constraint = CGSizeMake(label.frame.size.width, CGFloat.max)
        var size: CGSize
        let context = NSStringDrawingContext()
        let boundingBox = label.text!.boundingRectWithSize(constraint, options: .UsesLineFragmentOrigin, attributes: [NSFontAttributeName: label.font], context: context).size
        size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height))
        return size.height
        
    }
    
    func tapFunction(sender:UITapGestureRecognizer)
    {
        
//        print("success")
//        
//        for var y=10;y<self.imagearray1.count;y++
//        {
//            self.imagearray3.addObject(self.imagearray1[y])
//        }
       
        if(self.facebookandtwitter == 0)
        {
                  if((facebookuserdict.objectForKey(restaurantlabelname)) != nil)
                    {
                         self.loadinglabel.hidden=false
            
                        self.loading_indicator.hidden=false
                        self.loading_indicator.startAnimating()
                        self.backviewofactivity.hidden=false
                        
                         let fbuser1 = facebookuserdict.objectForKey(restaurantlabelname) as! NSNumber
                         let fbuser2 = String(fbuser1)
                         self.facebookfeeds(fbuser2)
            
                        
                         }
        }
        else
        {
            if(FHSTwitterEngine.sharedEngine().accessToken.key == nil)
            {
                let alert = UIAlertController(title: "Alert", message: "If you want to see feeds from Twitter,then authorize the app through login with Twitte", preferredStyle: UIAlertControllerStyle.Alert)
                let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
                    self.loadinglabel.hidden=true
                    self.loading_indicator.hidden=true
                    self.loading_indicator.stopAnimating()
                    self.backviewofactivity.hidden=true
                    
                    self.footerhidden=0
                    
                    self.heightoffooter=0
                    
                    self.collection_view.reloadData()
                }
                let okAction = UIAlertAction(title: "OK", style: .Default) { (action) in
                    let loginController = FHSTwitterEngine .sharedEngine() .loginControllerWithCompletionHandler { (Bool success) -> Void in
                        
                        print(success)
                        
                        let twittername1 = (self.twitteruserdict.objectForKey(self.restaurantlabelname)) as! String
                        
                        self.gettwitterposts(twittername1)
                        
                        self.loadinglabel.hidden=false
                        self.loading_indicator.hidden=false
                        self.loading_indicator.startAnimating()
                        self.backviewofactivity.hidden=false
                        
                        self.footerhidden=0
                        
                        self.heightoffooter=0
                        
                        self.collection_view.reloadData()
                        
                        //                        let obj = self.storyboard!.instantiateViewControllerWithIdentifier("gridvc") as! GridVC
                        //                        obj.namearray=self.namearray
                        //                        obj.sorteddistance1=self.sorteddistance
                        //                        self.navigationController?.pushViewController(obj, animated: true)
                        
                        } as UIViewController
                    self .presentViewController(loginController, animated: true, completion: nil)
                    
                }
                alert.addAction(cancelAction)
                alert.addAction(okAction)
                self.presentViewController(alert, animated: true, completion: nil)
            }
            else
            {
            self.loadinglabel.hidden=false
            self.loading_indicator.hidden=false
            self.loading_indicator.startAnimating()
            self.backviewofactivity.hidden=false
            
            let twittername1 = (twitteruserdict.objectForKey(restaurantlabelname)) as! String
            
            self.gettwitterposts(twittername1)
            
            self.footerhidden=0
            
            heightoffooter=0
            
            self.collection_view.reloadData()
            }
        }
        
        
    }
    
    func gettwitterposts(twitterusername:String)
    {
        let qualityOfServiceClass = QOS_CLASS_BACKGROUND
        let backgroundQueue = dispatch_get_global_queue(qualityOfServiceClass, 0)
        dispatch_async(backgroundQueue, {
            print("This is run on the background queue")
            let result = FHSTwitterEngine.sharedEngine().getTimelineForUser(twitterusername, isID: false, count: 10)
            print(result)
            if(result.count != 0)
            {
            for var i=0;i<10;i++
            {
                //INSTA FB AND TWITR POSTING LIKES AND COMMENTS POSTING DIFFERENCE ARRAY
                
                self.instafbtwitter.addObject("twitter")
                
                self.instacaptionarr.addObject(result[i].valueForKey("text")!)
                self.instacommentsarr.addObject(result[i].valueForKey("retweet_count")!)
                self.instalikesarr.addObject(result[i].valueForKey("favorite_count")!)
                
                // self.twitterimagesarr.addObject((result[i].valueForKey("extended_entities")?.valueForKey("media")?.valueForKey("media_url_https"))!)
                
                if((result[i].valueForKey("extended_entities")?.valueForKey("media")?.valueForKey("media_url_https")) != nil)
                {
                    self.twitterimagesarr.addObject((result[i].valueForKey("extended_entities")?.valueForKey("media")?.valueForKey("media_url_https"))![0]!)
                    
                }
                else
                {
                    self.twitterimagesarr.addObject("")
                }
                if(result[i].valueForKey("created_at") != nil)
                {
                    var dateoffeed = result[i].valueForKey("created_at") as! String
                    dateoffeed = dateoffeed.substringToIndex(dateoffeed.startIndex.advancedBy(10))
                self.instadatearray2.addObject(dateoffeed)
                }
                else
                {
                self.instadatearray2.addObject(" ")
                }
                //print(result[i].valueForKey("created_at"))
                
            }
            
            print(self.instacaptionarr.count)
            print(self.instacommentsarr.count)
            print(self.instalikesarr.count)
            print(self.twitterimagesarr)
            print(self.imagearray3.count)
            print(self.imagearray1.count)
            
            for var j=0;j<10;j++
            {
                var imageof = UIImage()
                if(self.twitterimagesarr[j] as! String == "")
                {
                    imageof = UIImage(named: "NoImage")!
                }
                else
                {
                    let url = NSURL(string: self.twitterimagesarr[j] as! String)
                    
                    let img=NSData(contentsOfURL: url!)
                    
                    imageof = UIImage(data: img!)!
                    
                }
                
                self.imagearray1.addObject(imageof)
                self.imagearray3.addObject(imageof)
                
            }
            print(self.imagearray3.count)
            print(self.imagearray1.count)
            self.twittercount=1
            
            }
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print("This is run on the main queue, after the previous code in outer block")
                self.footerhidden=0
                
                self.heightoffooter=0
                
                self.loadinglabel.hidden=true
                self.loading_indicator.hidden=true
                self.loading_indicator.stopAnimating()
                self.backviewofactivity.hidden=true
                
                self.collection_view.reloadData()
            })
        })
//        let result = FHSTwitterEngine.sharedEngine().getTimelineForUser(twitterusername, isID: false, count: 10)
//        print(result)
//        for var i=0;i<10;i++
//        {
//            
//            self.instacaptionarr.addObject(result[i].valueForKey("text")!)
//            self.instacommentsarr.addObject(result[i].valueForKey("retweet_count")!)
//            self.instalikesarr.addObject(result[i].valueForKey("favorite_count")!)
//            
//           // self.twitterimagesarr.addObject((result[i].valueForKey("extended_entities")?.valueForKey("media")?.valueForKey("media_url_https"))!)
//            
//            if((result[i].valueForKey("extended_entities")?.valueForKey("media")?.valueForKey("media_url_https")) != nil)
//            {
//            self.twitterimagesarr.addObject((result[i].valueForKey("extended_entities")?.valueForKey("media")?.valueForKey("media_url_https"))![0]!)
//            }
//            else
//            {
//            self.twitterimagesarr.addObject("")
//            }
//            
//            self.instadatearray2.addObject("date india")
//            
//        }
//        
//        print(self.instacaptionarr.count)
//        print(self.instacommentsarr.count)
//        print(self.instalikesarr.count)
//        print(self.twitterimagesarr)
//        print(imagearray3.count)
//        print(imagearray1.count)
//        
//        for var j=0;j<10;j++
//        {
//            var imageof = UIImage()
//            if(self.twitterimagesarr[j] as! String == "")
//            {
//            imageof = UIImage(named: "NoImage")!
//            }
//            else
//            {
//            let url = NSURL(string: self.twitterimagesarr[j] as! String)
//            
//            let img=NSData(contentsOfURL: url!)
//            
//            imageof = UIImage(data: img!)!
//                
//            }
//            
//            self.imagearray1.addObject(imageof)
//            self.imagearray3.addObject(imageof)
//            
//        }
//        print(imagearray3.count)
//        print(imagearray1.count)
//        twittercount=1
//        
//        self.footerhidden=0
//        
//        heightoffooter=0
//        
//        self.loadinglabel.hidden=true
//        
//        collection_view.reloadData()

    
    }
    
    func facebookfeeds(fbuserid:String)
    {
        let jsonUrlPath = "https://graph.facebook.com/\(fbuserid)/posts?fields=description,likes,picture,message,comments,created_time&access_token=EAAX1cYei8GABAN6GhRuF42ChJ6eyK76PGvs8UfbXMmEwEhzqZBhJfzKRQ9Ycw6l8jt184j9GkAmc12Tz7SPheIOgwRxPAc06CVrPZBWz6Sk6t0PPWU847XAMFPtuWpI4JFeGKETxw1po3a1AWcX6vVnTW2UDOvZBoaEHrBwuGwzaxcYLquQypCSmznpBxIOQsyzBT6fgqf47W1bqnTW"
        let url:NSURL = NSURL(string: jsonUrlPath)!
        
        let session  = NSURLSession .sharedSession()
        let task = session.dataTaskWithURL(url, completionHandler: {data,response,error -> Void in
            if error != nil
            {
                print(error!.localizedDescription)
            }
            
            var err:NSError?
            
            do
            {
                let resultdict = try NSJSONSerialization.JSONObjectWithData(data! , options: NSJSONReadingOptions.MutableContainers) as! NSMutableDictionary
                print(self.instaimgurl.count)
               // print(resultdict.valueForKey("data")![5].valueForKey("comments")?.valueForKey("data")?.count)
                print(self.idsofposts.count)
                print(resultdict.valueForKey("data")![0])
                for var i=0;i<10;i++
                {
                    //TEXT OF POST
                    
                    self.idsofposts.addObject(resultdict.valueForKey("data")![i].valueForKey("id")!)
                    if((resultdict.valueForKey("data")![i].objectForKey("message")) != nil)
                    {
                        
                        self.instacaptionarr.addObject(resultdict.valueForKey("data")![i]!.valueForKey("message")!)
                        
                    }
                    else
                    {
                    self.instacaptionarr.addObject("abcd")
                    }
                    
                    //DATE OF POST
                    
                    
                    var datestring = resultdict.valueForKey("data")![i]!.valueForKey("created_time")! as! String
                    datestring = datestring.substringToIndex(datestring.startIndex.advancedBy(10))
                    self.instadatearray2.addObject(datestring)
                    
                    //COMMENTCOUNT OF POST
                    
                    
                    if((resultdict.valueForKey("data")![i].objectForKey("comments")) != nil)
                    {
                        let commentcout1 = resultdict.valueForKey("data")![i].valueForKey("comments")?.valueForKey("data") as! NSArray
                        let commentcount2 = commentcout1.count
                        self.instacommentsarr.addObject(commentcount2)
                    }
                    else
                    {
                    self.instacommentsarr.addObject(0)
                    }
                    
                    //LIKECOUNT OF POST
                    
                    
                    if((resultdict.valueForKey("data")![i].objectForKey("likes")) != nil)
                    {
                        let likecount1 = resultdict.valueForKey("data")![i].valueForKey("likes")?.valueForKey("data") as! NSArray
                        let likecount2 = likecount1.count
                        
                        self.instalikesarr.addObject(likecount2)
                    }
                    else
                    {
                    self.instalikesarr.addObject(0)
                    }
                    
                    
                    //POST IMAGE URL
                    //picture
                    if((resultdict.valueForKey("data")![i].objectForKey("picture")) != nil)
                    {
                        let imagurl = resultdict.valueForKey("data")![i].objectForKey("picture")
                        self.facebookimageurl.addObject(imagurl!)
                    }
                    else
                    {
                    self.facebookimageurl.addObject("")
                    }
                    
                    
                    //INSTAFBTWITR ARRAY FOR POSTING LIKES AND COMMENTS
                    
                    
                    self.instafbtwitter.addObject("facebook")
                }
                print(self.facebookimageurl)
                for var j=0;j<10;j++
                {
                    
                    
                    if(self.facebookimageurl[j] as! String == "")
                    {
                    
                        let noimg = UIImage(named: "NoImage")
                        
                        self.imagearray1.addObject(noimg!)
                        self.imagearray3.addObject(noimg!)
                        
                    }
                    else
                    {
                    
                    let url = NSURL(string: self.facebookimageurl[j] as! String)
                    
                    let img=NSData(contentsOfURL: url!)
                        print(img)
                        var imageof = UIImage()
                       if(img == nil)
                       {
                        
                        imageof = UIImage(named: "NoImage")!
                        
                        }
                        else
                        {
                        
                        imageof = UIImage(data: img!)!
                        
                        }
                     
                        
                     self.imagearray1.addObject(imageof)
                        self.imagearray3.addObject(imageof)
                    
                    }
                    
                    
                }
                print(self.facebookimageurl.count)
                print(self.facebookimageurl)
                print(self.instalikesarr)
                print(self.instacommentsarr)
                print(self.instacaptionarr)
                print(self.instadatearray2.count)
                print(self.instadatearray2)
                self.twittercount=1
                self.collection_view.reloadData()
            } catch _
            {
                print("error ")
            }
            if err != nil
            {
                print("Json Error\(err!.localizedDescription)")
            }
            dispatch_async(dispatch_get_main_queue(),{
                
                if((self.twitteruserdict.objectForKey(self.restaurantlabelname)) != nil)
                {
                    self.facebookandtwitter=1
                    
                    self.footerhidden=1
                }
                else
                {
                    self.footerhidden=0
                    
                    self.heightoffooter=0

                }
                
                self.loadinglabel.hidden=true
                self.loading_indicator.hidden=true
                self.loading_indicator.stopAnimating()
                self.backviewofactivity.hidden=true
                
                self.collection_view.reloadData()
                
                
            })
        })
        
        task.resume()

    }
    @IBAction func likebuttonaction(sender: AnyObject)
    {
        
        print(sender.tag)
        let idofpost = self.idsofposts[sender.tag]
        
//        //let params = ["message": self.messagetopost]
//        print("str\(self,messagetopost)")
//        FBSDKGraphRequest(graphPath: "/\(idofpost)/likes", parameters: nil, HTTPMethod: "POST").startWithCompletionHandler({ (connection, result, error) -> Void in
//            if (error == nil){
//                
//                print(result)
//                print(FBSDKAccessToken.currentAccessToken().permissions)
//                
//            }
//            else
//            {
//                print(FBSDKAccessToken.currentAccessToken().permissions)
//                print(error)
//            }
//            
//        })
        var publishexist=""
        let permissionarray = FBSDKAccessToken.currentAccessToken().permissions
        // print(permissionarray)
        for i in permissionarray
        {
            print(i)
            if(i=="publish_actions")
            {
                publishexist = i as! String
            }
        }
        
        if(FBSDKAccessToken.currentAccessToken() == nil)
        {
            let alert = UIAlertController(title: "Alert", message: "Please Login with Facebook to Perform actions on Feeds", preferredStyle: UIAlertControllerStyle.Alert)
            let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
                
            }
            let okAction = UIAlertAction(title: "OK", style: .Default) { (action) in
                
                let loginManager : FBSDKLoginManager = FBSDKLoginManager()
                
                loginManager.logInWithReadPermissions(["email","user_posts"]) { (result, error) -> Void in
                    
                    if (error != nil)
                    {
                        print("error")
                    }
                    else if (result.isCancelled)
                    {
                        NSLog("Cancelled");
                    }
                    else
                    {
                        if ((FBSDKAccessToken.currentAccessToken()) != nil)
                        {
                            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).startWithCompletionHandler({ (connection, result, error) -> Void in
                                print(FBSDKAccessToken.currentAccessToken().permissions)
                                
                                if (error == nil)
                                {
                                    
                                }
                                
                            })
                        }
                    }
                }
                
            }
            alert.addAction(cancelAction)
            alert.addAction(okAction)
            self.presentViewController(alert, animated: true, completion: nil)
            
        }
        else if(publishexist == "")
        {
            let idofpost = self.idsofposts[sender.tag]
            
            let loginManager : FBSDKLoginManager = FBSDKLoginManager()
            
            loginManager.logInWithPublishPermissions(["publish_actions"]) { (result, error) -> Void in
                
                let params = ["message": "hi"]
                
//                FBSDKGraphRequest(graphPath: "/\(idofpost)/likes", parameters: nil, HTTPMethod: "POST").startWithCompletionHandler({ (connection, result, error) -> Void in
//                    if (error == nil){
//                        
//                        print(result)
//                        print(FBSDKAccessToken.currentAccessToken().permissions)
//                        
//                    }
//                    else
//                    {
//                        print(FBSDKAccessToken.currentAccessToken().permissions)
//                        print(error)
//                    }
//                    
//                })
                
            }
        }
        else
        {
            
            let idofpost = self.idsofposts[sender.tag]
            
            let params = ["message": self.messagetopost]
            print("str\(self,messagetopost)")
            FBSDKGraphRequest(graphPath: "/\(idofpost)/likes", parameters: nil, HTTPMethod: "POST").startWithCompletionHandler({ (connection, result, error) -> Void in
                if (error == nil){
                    
                    print(result)
                    print(FBSDKAccessToken.currentAccessToken().permissions)
                    
                }
                else
                {
                    print(FBSDKAccessToken.currentAccessToken().permissions)
                    print(error)
                }
                
            })
            
            
        }
    }
    @IBAction func commentbuttonaction(sender: AnyObject)
    {
        self.indexofselectedrow=sender.tag
        self.collection_view.reloadData()
        print("comment\(sender.tag)")
        
    }
    @IBAction func postbutton(sender: AnyObject) {
        
        if(self.instafbtwitter[sender.tag] as! String == "instagram")
        {
        
        }
        else if(self.instafbtwitter[sender.tag] as! String == "facebook")
        {
        
        
        
        var publishexist=""
        
        let permissionarray = FBSDKAccessToken.currentAccessToken().permissions
        // print(permissionarray)
        for i in permissionarray
        {
            print(i)
            if(i=="publish_actions")
            {
                publishexist = i as! String
            }
        }

        if(FBSDKAccessToken.currentAccessToken() == nil)
        {
            let alert = UIAlertController(title: "Alert", message: "Please Login with Facebook to Perform actions on Feeds", preferredStyle: UIAlertControllerStyle.Alert)
            let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
                
            }
            let okAction = UIAlertAction(title: "OK", style: .Default) { (action) in
                
                let loginManager : FBSDKLoginManager = FBSDKLoginManager()
                
                loginManager.logInWithReadPermissions(["email","user_posts"]) { (result, error) -> Void in
                    
                    if (error != nil)
                    {
                        print("error")
                    }
                    else if (result.isCancelled)
                    {
                        NSLog("Cancelled");
                    }
                    else
                    {
                        if ((FBSDKAccessToken.currentAccessToken()) != nil)
                        {
                            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).startWithCompletionHandler({ (connection, result, error) -> Void in
                                print(FBSDKAccessToken.currentAccessToken().permissions)
                                
                                if (error == nil)
                                {
                                    
                                }
                                
                            })
                        }
                    }
                }
                
        }
        alert.addAction(cancelAction)
            alert.addAction(okAction)
            self.presentViewController(alert, animated: true, completion: nil)
            
    }
        else if(publishexist == "")
        {
            let idofpost = self.idsofposts[sender.tag]
            
            let loginManager : FBSDKLoginManager = FBSDKLoginManager()
            
            loginManager.logInWithPublishPermissions(["publish_actions"]) { (result, error) -> Void in
                
                let params = ["message": "hi"]
                
                FBSDKGraphRequest(graphPath: "/\(idofpost)/comments", parameters: params, HTTPMethod: "POST").startWithCompletionHandler({ (connection, result, error) -> Void in
                    if (error == nil){
                        
                        print(result)
                        print(FBSDKAccessToken.currentAccessToken().permissions)
                        
                    }
                    else
                    {
                        print(FBSDKAccessToken.currentAccessToken().permissions)
                        print(error)
                    }
                    
                })
                
            }
        }
        else
        {
            let idofpost = self.idsofposts[sender.tag]
            
             let params = ["message": self.messagetopost]
            print("str\(self,messagetopost)")
            FBSDKGraphRequest(graphPath: "/\(idofpost)/comments", parameters: params, HTTPMethod: "POST").startWithCompletionHandler({ (connection, result, error) -> Void in
                if (error == nil){
                    
                    print(result)
                    print(FBSDKAccessToken.currentAccessToken().permissions)
                    
                }
                else
                {
                    print(FBSDKAccessToken.currentAccessToken().permissions)
                    print(error)
                }
                
            })
        }
        }
        else
        {
        
        }
        
    }
   /* func facebookurlhit()
    {
       
        
        if(facebookurlcount==1)
        {
           if(facebookimageurlcount<10)
           {
            
            self.facebookimageurl(self.facebookidarray[facebookimageurlcount] as! NSString)
            
            }
            else
           {
          //
            self.insfbtwittercount2=1
            print(self.imagearray3.count)
            print(imagearray1.count)
            print(self.instacaptionarr.count)
            print(self.instacommentsarr.count)
            print(self.instalikesarr.count)
            print(self.instadatearray2.count)
            self.collection_view.reloadData()
            }
        }
        else
        {
        facebookid()
        }
        
    }

    func facebookid()
    {
        
        let jsonUrlPath = "https://graph.facebook.com/SaltandStraw/posts?access_token=EAADIn2EiUm4BAO3gkGoiEsJAblbXw0Yb4Pb2yfBjPALnNBupVNTE7lLiYRx3t4KSbUtRZBmPvKT9CgvkiF204JZBvsH2sVpfZAfCv2fL8UY918cnZCHFcGICO8YAeV5ZBnxUSuAGTeWtCMnUxWmiUyWnE3Mef18GZCe9T3IcV2QOwu1dY2xJ7gpwZApGwxykWLLk6y0K3bjOh3V2HqnGACXFhucwRHcOLpRHVwNTfvCKQZDZD"
        let url:NSURL = NSURL(string: jsonUrlPath)!
        
        let session  = NSURLSession .sharedSession()
        let task = session.dataTaskWithURL(url, completionHandler: {data,response,error -> Void in
            if error != nil
            {
                print(error!.localizedDescription)
            }
            
            var err:NSError?
            
            do
            {
                self.facebookidresponse = try NSJSONSerialization.JSONObjectWithData(data! , options: NSJSONReadingOptions.MutableContainers) as! NSMutableDictionary
                
                print(self.facebookidresponse.valueForKey("data"))
                print(self.facebookidresponse.valueForKey("data")![0].valueForKey("message"))
                for var i = 0;i<self.facebookidresponse.valueForKey("data")?.count;i++
                {
                
                    if(((self.facebookidresponse.valueForKey("data")![i])?.valueForKey("message")) != nil)
                    {
                        
                        self.facebookidarray.addObject(self.facebookidresponse.valueForKey("data")![i].valueForKey("id")!)
                        self.facebookdatearray.addObject(self.facebookidresponse.valueForKey("data")![i].valueForKey("created_time")!)
                        self.facebookmessagearray.addObject(self.facebookidresponse.valueForKey("data")![i].valueForKey("message")!)
                        
                    }
                    else
                    {
                    
                    }

                }
                
                print(self.facebookidarray.count)
                print(self.facebookdatearray.count)
                print(self.facebookmessagearray.count)
              
                self.facebookurlcount=1
           
                self.facebookurlhit()
                
            } catch _
            {
                print("error ")
            }
            
            
            
            if err != nil
            {
                print("Json Error\(err!.localizedDescription)")
            }
            dispatch_async(dispatch_get_main_queue(),{
                
              //  self.collection_view.reloadData()
            })
            
        })
        
        task.resume()
    }
    func facebookimageurl(idofpost: NSString)
    {
        print(idofpost)
        let jsonUrlPath = "https://graph.facebook.com/v2.7/\(idofpost)?fields=attachments&access_token=EAADIn2EiUm4BAO3gkGoiEsJAblbXw0Yb4Pb2yfBjPALnNBupVNTE7lLiYRx3t4KSbUtRZBmPvKT9CgvkiF204JZBvsH2sVpfZAfCv2fL8UY918cnZCHFcGICO8YAeV5ZBnxUSuAGTeWtCMnUxWmiUyWnE3Mef18GZCe9T3IcV2QOwu1dY2xJ7gpwZApGwxykWLLk6y0K3bjOh3V2HqnGACXFhucwRHcOLpRHVwNTfvCKQZDZD"
        let url:NSURL = NSURL(string: jsonUrlPath)!
        
        let session  = NSURLSession .sharedSession()
        let task = session.dataTaskWithURL(url, completionHandler: {data,response,error -> Void in
            if error != nil
            {
                print(error!.localizedDescription)
            }
            
            var err:NSError?
            
            do
            {
                
                self.facebookimageres = try NSJSONSerialization.JSONObjectWithData(data! , options: NSJSONReadingOptions.MutableContainers) as! NSMutableDictionary
                
                print(self.facebookimageres.valueForKey("attachments")?.valueForKey("data")![0].valueForKey("media")?.valueForKey("image")?.valueForKey("src"))
                
               // self.imagearray3.addObject(<#T##anObject: AnyObject##AnyObject#>)
                var url=NSURL()
                if(self.facebookimageres.valueForKey("attachments")?.valueForKey("data")![0].valueForKey("media")?.valueForKey("image")?.valueForKey("src") == nil)
                {
                url=NSURL(string: "https://scontent.xx.fbcdn.net/v/t1.0-9/p720x720/14222184_1073294516058164_7698371213839146430_n.jpg?oh=5b74d3fcd977dc775e6d11bd964ceef6&oe=58ACF6AE")!
                }
                else
                {
                    
                    url=NSURL(string: (self.facebookimageres.valueForKey("attachments")?.valueForKey("data")![0].valueForKey("media")?.valueForKey("image")?.valueForKey("src"))! as! String)!
                    
                }
                    let img=NSData(contentsOfURL: url)
                    
                    let imageof = UIImage(data: img!)
                    self.imagearray3.addObject(imageof!)
                    self.imagearray1.addObject(imageof!)
            
            self.instacaptionarr.addObject("text1 hdgdg jk dgj dgj kdgj dgj kdgjk dgj kdg deg jk dgj kdg ")
                self.instacommentsarr.addObject(1)
                self.instalikesarr.addObject(2)
                self.instadatearray2.addObject("date of post")
                self.facebookimageurlcount++
                
                self.facebookurlhit()
                
            } catch _
            {
                print("error ")
            }
            
            
            
            if err != nil
            {
                print("Json Error\(err!.localizedDescription)")
            }
            dispatch_async(dispatch_get_main_queue(),{
                
               // self.collection_view.reloadData()
            })
        })
        
        task.resume()
 
    }
    
    func facebooklikecount()
    {
        let jsonUrlPath = "https://graph.facebook.com/167185396669085_1073505806037035/likes?access_token=EAADIn2EiUm4BAO3gkGoiEsJAblbXw0Yb4Pb2yfBjPALnNBupVNTE7lLiYRx3t4KSbUtRZBmPvKT9CgvkiF204JZBvsH2sVpfZAfCv2fL8UY918cnZCHFcGICO8YAeV5ZBnxUSuAGTeWtCMnUxWmiUyWnE3Mef18GZCe9T3IcV2QOwu1dY2xJ7gpwZApGwxykWLLk6y0K3bjOh3V2HqnGACXFhucwRHcOLpRHVwNTfvCKQZDZD"
        let url:NSURL = NSURL(string: jsonUrlPath)!
        
        let session  = NSURLSession .sharedSession()
        let task = session.dataTaskWithURL(url, completionHandler: {data,response,error -> Void in
            if error != nil
            {
                print(error!.localizedDescription)
            }
            
            var err:NSError?
            
            do
            {
                self.facebooklikecountdict = try NSJSONSerialization.JSONObjectWithData(data! , options: NSJSONReadingOptions.MutableContainers) as! NSMutableDictionary
                
                print(self.facebooklikecountdict.valueForKey("data"))
                print(self.facebooklikecountdict.valueForKey("data")?.count)
                self.facebooklikecountarray.addObject((self.facebooklikecountdict.valueForKey("data")?.count)!)
                
            } catch _
            {
                print("error ")
            }
            
            
            
            if err != nil
            {
                print("Json Error\(err!.localizedDescription)")
            }
            dispatch_async(dispatch_get_main_queue(),{
                
                
            })
        })
        
        task.resume()

    }
    
    func facebookcommentcount()
    {
        let jsonUrlPath = "https://graph.facebook.com/167185396669085_1073505806037035/comments?access_token=EAADIn2EiUm4BAO3gkGoiEsJAblbXw0Yb4Pb2yfBjPALnNBupVNTE7lLiYRx3t4KSbUtRZBmPvKT9CgvkiF204JZBvsH2sVpfZAfCv2fL8UY918cnZCHFcGICO8YAeV5ZBnxUSuAGTeWtCMnUxWmiUyWnE3Mef18GZCe9T3IcV2QOwu1dY2xJ7gpwZApGwxykWLLk6y0K3bjOh3V2HqnGACXFhucwRHcOLpRHVwNTfvCKQZDZD"
        let url:NSURL = NSURL(string: jsonUrlPath)!
        
        let session  = NSURLSession .sharedSession()
        let task = session.dataTaskWithURL(url, completionHandler: {data,response,error -> Void in
            if error != nil
            {
                print(error!.localizedDescription)
            }
            
            var err:NSError?
            
            do
            {
                self.facebookcommentcountdict = try NSJSONSerialization.JSONObjectWithData(data! , options: NSJSONReadingOptions.MutableContainers) as! NSMutableDictionary
                
                print(self.facebookcommentcountdict.valueForKey("data"))
                print(self.facebookcommentcountdict.valueForKey("data")?.count)
                self.facebookcommentcountarray.addObject((self.facebookcommentcountdict.valueForKey("data")?.count)!)
                print(self.facebookcommentcountarray)
            } catch _
            {
                print("error ")
            }
            
            
            
            if err != nil
            {
                print("Json Error\(err!.localizedDescription)")
            }
            dispatch_async(dispatch_get_main_queue(),{
                
                
            })
        })
        
        task.resume()

    }*/
    
    
}
//https://graph.facebook.com/v2.7/107898832590939_1142665119114300?fields=attachments&access_token=
//https://graph.facebook.com/167185396669085_1073505806037035/likes?access_token=


//
//if ([kind isEqualToString:CHTCollectionElementKindSectionHeader]) {
//    reusableView = [collectionView dequeueReusableSupplementaryViewOfKind:kind
//        withReuseIdentifier:HEADER_IDENTIFIER
//        forIndexPath:indexPath];
//} else if ([kind isEqualToString:CHTCollectionElementKindSectionFooter]) {
//    reusableView = [collectionView dequeueReusableSupplementaryViewOfKind:kind
//        withReuseIdentifier:FOOTER_IDENTIFIER
//        forIndexPath:indexPath];
//}
//


//https://api.instagram.com/v1/media/1349506865558336465_599894690/comments?access_token=3079201992.e029fea.59a10e100f4f45dbb208ae3ee54e9f94&text=test








