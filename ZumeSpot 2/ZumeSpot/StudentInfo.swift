//
//  StudentInfo.swift
//  DataBaseDemo
//
//  Created by Krupa-iMac on 05/08/14.
//  Copyright (c) 2014 TheAppGuruz. All rights reserved.
//

import UIKit

class StudentInfo: NSObject
{
    
    var Name: String = String()
    var Latitude: Double = Double()

    var Longitude: Double = Double()
    var Randomno: String = String()
    
    var PathName: String = String()
    var Type: String = String()
    var Desc: String = String()
    var Title: String = String()
    var Thumbnailname: String=String()
    var Firstname: String = String()
    
}
