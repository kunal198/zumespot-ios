//
//  ModelManager.swift
//  DataBaseDemo
//
//  Created by Krupa-iMac on 05/08/14.
//  Copyright (c) 2014 TheAppGuruz. All rights reserved.
//

import UIKit

let sharedInstance = ModelManager()

class ModelManager: NSObject
{
    
    var database: FMDatabase? = nil

    class func getInstance() -> ModelManager
    {
        if(sharedInstance.database == nil)
        {
            sharedInstance.database = FMDatabase(path: Util.getPath("HotelDatabase.sqlite"))
        }
        return sharedInstance
    }
    
    func getAllhotelsname() -> NSMutableArray {
        sharedInstance.database!.open()
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("SELECT * FROM hoteldata", withArgumentsInArray: nil)
        let marrStudentInfo : NSMutableArray = NSMutableArray()
        if (resultSet != nil)
        {
            while resultSet.next()
            {
                let studentInfo : StudentInfo = StudentInfo()
                studentInfo.Name = resultSet.stringForColumn("name") as String

                marrStudentInfo.addObject(studentInfo.Name)
            }
            
        }
        sharedInstance.database!.close()
        return marrStudentInfo
    }
    
    func getAllhotelslatitude() -> NSMutableArray
    {
        sharedInstance.database!.open()
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("SELECT * FROM hoteldata", withArgumentsInArray: nil)
        let marrStudentInfo : NSMutableArray = NSMutableArray()
        if (resultSet != nil)
        {
            while resultSet.next()
            {
                let studentInfo : StudentInfo = StudentInfo()
                studentInfo.Latitude = resultSet.doubleForColumn("latitude") as Double
                
                marrStudentInfo.addObject(studentInfo.Latitude)
                
            }
        }
        sharedInstance.database!.close()
        return marrStudentInfo
    }
    
    func getAllhotelslongitude() -> NSMutableArray {
        sharedInstance.database!.open()
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("SELECT * FROM hoteldata", withArgumentsInArray: nil)
        let marrStudentInfo : NSMutableArray = NSMutableArray()
        if (resultSet != nil)
        {
            while resultSet.next()
            {
                let studentInfo : StudentInfo = StudentInfo()
                studentInfo.Latitude = resultSet.doubleForColumn("longitude") as Double
                
                marrStudentInfo.addObject(studentInfo.Latitude)
            }
        }
        sharedInstance.database!.close()
        return marrStudentInfo
        
    }
    
}
