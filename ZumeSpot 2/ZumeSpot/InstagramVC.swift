//
//  InstagramVC.swift
//  ZumeSpot
//
//  Created by mrinal khullar on 9/1/16.
//  Copyright © 2016 mrinal khullar. All rights reserved.
//

import UIKit

class InstagramVC: UIViewController, UIWebViewDelegate{

    
    var namearray=NSMutableArray()
    
     let appdele=AppDelegate()
    
    @IBOutlet weak var web_view: UIWebView!
    
    @IBOutlet weak var activityindi: UIActivityIndicatorView!
    
    var kBaseURL = "https://instagram.com/"
    var kAuthenticationURL="oauth/authorize/?client_id=%@&redirect_uri=%@&response_type=token&scope=likes+comments+basic"
    var kClientID = "42a1a7203338418d8c533df07a78737f"
  //  var kRedirectURI="http://dev1.businessprodemo.com/Slipperyslickproductions/php/"
    var kRedirectURI="http://www.brihaspatitech.com"
    var kAccessToken="access_token"
    var typeOfAuthentication = ""

    var sorteddistance=[AnyObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
     override func viewWillAppear(animated: Bool)
     {UIApplication.sharedApplication().statusBarHidden = true
        
       
        
        var authURL: String? = nil
        if (typeOfAuthentication == "UNSIGNED") {
            authURL = "\(appdele.INSTAGRAM_AUTHURL)?client_id=\(appdele.INSTAGRAM_CLIENT_ID)&redirect_uri=\(appdele.INSTAGRAM_REDIRECT_URI)&response_type=token&scope=\(appdele.INSTAGRAM_SCOPE)&DEBUG=True"
        }
        else {
            authURL = "\(appdele.INSTAGRAM_AUTHURL)?client_id=\(appdele.INSTAGRAM_CLIENT_ID)&redirect_uri=\(appdele.INSTAGRAM_REDIRECT_URI)&response_type=code&scope=\(appdele.INSTAGRAM_SCOPE)&DEBUG=True"
        }
        web_view.loadRequest(NSURLRequest(URL: NSURL(string: authURL!)!))
        web_view.delegate = self
    }
    
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool
    {
//        let urlString = request.URL!.absoluteString
//        let Url = request.URL!
//        let UrlParts = Url.pathComponents!
//        if UrlParts.count == 1 {
//            let tokenParam = (urlString as NSString).rangeOfString(kAccessToken)
//            if tokenParam.location != NSNotFound {
//                var token = urlString.substringFromIndex(urlString.startIndex.advancedBy(NSMaxRange(tokenParam)))
//                // If there are more args, don't include them in the token:
//                let endRange = (token as NSString).rangeOfString("&")
//                if endRange.location != NSNotFound {
//                    token = token.substringToIndex(token.startIndex.advancedBy(endRange.location))
//                }
//                if token.characters.count > 0 {
//                    // call the method to fetch the user's Instagram info using access token
//                   // gAppData.getUserInstagramWithAccessToken(token)
//                }
//            }
//            else {
//                print("rejected case, user denied request")
//            }
//            return false
//        }
        return self.checkRequestForCallbackURL(request)

        
    }
    
    func webViewDidStartLoad(webView: UIWebView) {
        activityindi.startAnimating()
        activityindi.hidden = false
        web_view.layer.removeAllAnimations()
        web_view.userInteractionEnabled = false
        UIView.animateWithDuration(0.1, animations: {() -> Void in
            //  loginWebView.alpha = 0.2;
        })
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        activityindi.stopAnimating()
        activityindi.hidden = true
        web_view.layer.removeAllAnimations()
        web_view.userInteractionEnabled = true
        UIView.animateWithDuration(0.1, animations: {() -> Void in
            //loginWebView.alpha = 1.0;
        })
    }
    
    func webView(webView: UIWebView, didFailLoadWithError error: NSError?) {
//        print("Code : %d \nError : %@", error!.code, error!.description)
//        //Error : Error Domain=WebKitErrorDomain Code=102 "Frame load interrupted"
//        if error!.code == 102 {
//            return
//        }
//        if error!.code == -1009 || error!.code == -1005 {
//            //        _completion(kNetworkFail,kPleaseCheckYourInternetConnection);
//        }
//        else {
//            //        _completion(kError,error.description);
//        }
//      //  UIUtils.networkFailureMessage()
        self.webViewDidFinishLoad(webView)

    }
    
    
    func checkRequestForCallbackURL(request: NSURLRequest) -> Bool {
        let urlString = request.URL!.absoluteString
        if (typeOfAuthentication == "UNSIGNED") {
            // check, if auth was succesfull (check for redirect URL)
            if urlString.hasPrefix(appdele.INSTAGRAM_REDIRECT_URI) {
                // extract and handle access token
                let range = (urlString as NSString).rangeOfString("#access_token=")
                self.handleAuth(urlString.substringFromIndex(urlString.startIndex.advancedBy(range.location + range.length)))
                return false
            }
        }
        else {
            if urlString.hasPrefix(appdele.INSTAGRAM_REDIRECT_URI) {
                // extract and handle access token
                let range = (urlString as NSString).rangeOfString("code=")
                self.makePostRequest(urlString.substringFromIndex(urlString.startIndex.advancedBy(range.location + range.length)))
                return false
            }
        }
        return true
    }
    
    func makePostRequest(code: String)
    {
        
        let post = "client_id=\(appdele.INSTAGRAM_CLIENT_ID)&client_secret=\(appdele.INSTAGRAM_CLIENTSERCRET)&grant_type=authorization_code&redirect_uri=\(appdele.INSTAGRAM_REDIRECT_URI)&code=\(code)"
        let postData = post.dataUsingEncoding(NSASCIIStringEncoding, allowLossyConversion: true)!
        let postLength = "\(UInt(postData.length))"
        let requestData = NSMutableURLRequest(URL: NSURL(string: "https://api.instagram.com/oauth/access_token")!)
        requestData.HTTPMethod = "POST"
        requestData.setValue(postLength, forHTTPHeaderField: "Content-Length")
        requestData.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        requestData.HTTPBody = postData
        
       // var response: NSURLResponse? = nil
        //var responseData = try! NSURLConnection.sendSynchronousRequest(requestData, returningResponse: response)!
        var urldata=NSData()
        var requestError: NSError? = nil
        let response : AutoreleasingUnsafeMutablePointer<NSURLResponse?> = nil
        do{
            let urlData = try NSURLConnection.sendSynchronousRequest(requestData, returningResponse: response)
            urldata=urlData
            print(urlData)
        }
        catch (let e) {
            print(e)
        }
        let dict = try! NSJSONSerialization.JSONObjectWithData(urldata, options: NSJSONReadingOptions.AllowFragments)
        print(dict)
        self.handleAuth((dict.valueForKey("access_token") as! String))
        
    }
    func handleAuth(authToken: String)
    {
        print("successfully logged in with Tocken == \(authToken)")
       // self.dismissViewControllerAnimated(true, completion: { _ in })
       // print("permissions of instagram  \(authToken)")
        
        let obj = self.storyboard!.instantiateViewControllerWithIdentifier("gridvc") as! GridVC
        obj.namearray=self.namearray
        obj.sorteddistance1=self.sorteddistance
        obj.accesstoken = authToken
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
}
