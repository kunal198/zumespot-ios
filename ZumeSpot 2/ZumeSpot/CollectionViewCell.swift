//
//  CollectionViewCell.swift
//  ZumeSpot
//
//  Created by mrinal khullar on 8/23/16.
//  Copyright © 2016 mrinal khullar. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell
{
    
    @IBOutlet var image_view: UIImageView!
    @IBOutlet weak var namelabel: UILabel!
    @IBOutlet weak var distancelabel: UILabel!
    
}
