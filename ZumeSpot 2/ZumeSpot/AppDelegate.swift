//
//  AppDelegate.swift
//  ZumeSpot
//
//  Created by mrinal khullar on 8/22/16.
//  Copyright © 2016 mrinal khullar. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{

    var window: UIWindow?
    
    let INSTAGRAM_AUTHURL = "https://api.instagram.com/oauth/authorize/"
    let INSTAGRAM_APIURl = "https://api.instagram.com/v1/users/"
    let INSTAGRAM_CLIENT_ID = "42a1a7203338418d8c533df07a78737f"
    let INSTAGRAM_CLIENTSERCRET = "05c59ded562c4ac0b82a76dbd70c3fe1"
    let INSTAGRAM_REDIRECT_URI = "http://www.brihaspatitech.com"
    let INSTAGRAM_ACCESS_TOKEN = "access_token"
    let INSTAGRAM_SCOPE = "likes+comments+relationships+public_content"

    var instagramvariable=0
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool
    {
        
        // Override point for customization after application launch.
        FBSDKLoginButton.classForCoder()
        Util.copyFile("HotelDatabase.sqlite")
        
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
    }
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool
    {
        if (FBSDKApplicationDelegate.sharedInstance() != nil)
        {
            return FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
        }
        return true
    }
    func applicationWillResignActive(application: UIApplication)
    {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
        
    }

    func applicationDidEnterBackground(application: UIApplication)
    {
        
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
    }

    func applicationWillEnterForeground(application: UIApplication)
    {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        
    }

    func applicationDidBecomeActive(application: UIApplication)
    {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        FBSDKAppEvents.activateApp()
    }

    func applicationWillTerminate(application: UIApplication)
    {
        
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        let loginManager: FBSDKLoginManager = FBSDKLoginManager()
        loginManager.logOut()
       
    }


}

